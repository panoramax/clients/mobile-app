part of panoramax;

class SequenceListItem extends StatefulWidget {
  const SequenceListItem({super.key, required this.sequenceDetails, this.playAnimationOnStart = false});

  final SequenceCardDetails sequenceDetails;
  final bool playAnimationOnStart;

  @override
  State<SequenceListItem> createState() => _SequenceListItemState();
}

class _SequenceListItemState extends State<SequenceListItem> with TickerProviderStateMixin {
  late final _slidableController = SlidableController(this);

  @override
  void initState() {
    super.initState();
    if (this.widget.playAnimationOnStart) {
      Future.delayed(Duration(milliseconds: 500), () {
        _slidableController.openStartActionPane();
        Future.delayed(Duration(seconds: 1), () {
          _slidableController.close();
        });
      });
    }
  }

  @override
  void dispose() {
    _slidableController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(8),
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.tertiaryContainer,
          borderRadius: BorderRadius.circular(7.5),
          border: Border.all(
            color: Theme.of(context).colorScheme.primaryContainer,
            style: BorderStyle.solid,
            width: 2
          )
        ),
        child: FutureBuilder<RemoteSequenceStatus>(
            future: widget.sequenceDetails.getRemoteStatus(),
            builder: (BuildContext context,
                AsyncSnapshot<RemoteSequenceStatus> remoteStatusSnapshot) {
              String numberOfElementText = widget
                  .sequenceDetails.numberOfPublishedPictures
                  .toString() +
                  " / " +
                  widget.sequenceDetails.numberOfLocalPictures.toString() +
                  " " +
                  '${AppLocalizations.of(context)!.publishedElements(widget.sequenceDetails.numberOfPublishedPictures)}';
              bool isShareable =
                  remoteStatusSnapshot.data == RemoteSequenceStatus.READY;
              bool isEditable = remoteStatusSnapshot.data ==
                  RemoteSequenceStatus.PARTIALLY_SENT ||
                  remoteStatusSnapshot.data == RemoteSequenceStatus.NOT_SENT;
              return Slidable(
                  key: ValueKey(widget.key),
                  controller: _slidableController,
                  enabled: isShareable || isEditable,
                  startActionPane: ActionPane(
                    extentRatio: 0.25,
                    motion: const DrawerMotion(),
                    children: [
                      if (isEditable) EditButton(context),
                      if (isShareable) ShareButton(context),
                    ],
                  ),
                  child: Container(
                      padding: EdgeInsets.all(8.5),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          spacing: 10,
                          children: [
                            SequenceThumbnail(
                              context,
                              remoteStatusSnapshot
                            ),
                            SequenceDetails(
                              context,
                              remoteStatusSnapshot,
                              numberOfElementText
                            )
                          ]
                      )
                  )
              );
            }
        )
    );
  }

  Widget ShareButton(BuildContext context) {
    return SlidableAction(
      onPressed: (BuildContext context) {
        shareUrl(context, widget.sequenceDetails.remoteId!);
      },
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      icon: Icons.share_outlined,
    );
  }

  Widget EditButton(BuildContext context) {
    return SlidableAction(
      onPressed: (BuildContext context) {
        HomepageService.goToCapture(
            sequenceToEditId: (widget.sequenceDetails
                    as PublicationIncompleteSequenceCardDetails)
                .localDetails
                .id);
      },
      backgroundColor:
          Theme.of(context).colorScheme.primaryContainer,
        icon: Icons.mode_edit_outline_outlined);
  }

  Expanded SequenceDetails(
      BuildContext context,
      AsyncSnapshot<RemoteSequenceStatus> remoteStatusSnapshot,
      String numberOfElementText) {
    return Expanded(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(widget.sequenceDetails.title,
        style: Theme.of(context)
          .textTheme
          .titleMedium!
          .apply(fontSizeFactor: 1.05, fontWeightDelta: 1)
      ),
      Tooltip(
          message: AppLocalizations.of(context)!.shotDate,
          child: Row(children: [
            Icon(
              Icons.photo_camera,
              size: 20,
            ),
            SizedBox(width: 5),
            Text(dateFormat.format(widget.sequenceDetails.creationDate))
          ])),
      if (widget.sequenceDetails.publicationDate != null)
        Tooltip(
            message: AppLocalizations.of(context)!.publishedDate,
            child: Row(children: [
              Icon(
                Icons.publish,
                size: 20,
              ),
              SizedBox(width: 5),
              Text(dateFormat.format(widget.sequenceDetails.publicationDate!))
            ])),
      if (remoteStatusSnapshot.hasData)
        Row(children: [
          if (remoteStatusSnapshot.data == RemoteSequenceStatus.READY)
            Icon(
              Icons.task_alt,
              color: Colors.green,
            ),
          if (remoteStatusSnapshot.data == RemoteSequenceStatus.NOT_SENT)
            Icon(
              Icons.priority_high,
              color: Colors.red,
            ),
          if (remoteStatusSnapshot.data == RemoteSequenceStatus.PARTIALLY_SENT)
            Icon(
              Icons.warning_amber,
              color: Colors.orange,
            ),
          SizedBox(width: 4),
          Text(numberOfElementText,
              style: Theme.of(context)
                  .textTheme
                  .labelLarge!
                  .apply(fontWeightDelta: 1))
        ])
      else
        LinearProgressIndicator(
            backgroundColor: Colors.white,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent))
    ]));
  }

  Widget SequenceThumbnail(BuildContext context,
      AsyncSnapshot<RemoteSequenceStatus> remoteStatusSnapshot) {
    return Material(
        elevation: 1,
        borderRadius: BorderRadius.circular(8.0),
        child: SizedBox(
          width: 80,
          height: 80,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: FutureBuilder<Image?>(
                future: widget.sequenceDetails.getThumbnail(),
                builder:
                    (BuildContext context, AsyncSnapshot<Image?> imageSnapshot) {
                  if (imageSnapshot.hasData && remoteStatusSnapshot.hasData && imageSnapshot.data != null) {
                    if (remoteStatusSnapshot.data ==
                        RemoteSequenceStatus.BLURRING) {
                      return SequenceBlurringThumbnail(context);
                    } else if (remoteStatusSnapshot.data ==
                        RemoteSequenceStatus.HIDDEN) {
                      return SequenceHiddenThumbnail(context, imageSnapshot.data!);
                    } else {
                      return imageSnapshot.data!;
                    }
                  } else {
                    return TemporaryContentWithBorder(context,
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                        ));
                  }
                }),
          ),
        ));
  }

  Container SequenceHiddenThumbnail(
      BuildContext context, Image image) {
    return TemporaryContentWithBorder(
      context,
      child: Stack(
        children: [
          Container(
              height: double.infinity,
              width: double.infinity,
              child: image),
          Container(
            height: double.infinity,
            width: double.infinity,
            padding: const EdgeInsets.all(5.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              gradient: RadialGradient(
                colors: <Color>[Colors.black26, Colors.black54],
              ),
            ),
            child: Text(
              AppLocalizations.of(context)!.hidden,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .apply(color: Colors.white, fontWeightDelta: 1),
            ),
          ),
        ],
      ),
    );
  }

  Container SequenceBlurringThumbnail(BuildContext context) {
    return TemporaryContentWithBorder(context,
        child: Text(
          AppLocalizations.of(context)!.blurringInProgress,
          textAlign: TextAlign.center,
          style:
              Theme.of(context).textTheme.bodySmall!.apply(fontWeightDelta: 1),
        ));
  }

  Container TemporaryContentWithBorder(BuildContext context,
      {required Widget child}) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.tertiaryContainer,
          borderRadius: BorderRadius.circular(7.5),
          border: Border.all(
              color: Theme.of(context).colorScheme.primaryContainer,
              style: BorderStyle.solid,
              width: 1)),
      child: Center(
        child: child,
      ),
    );
  }

  Future<void> shareUrl(BuildContext context, String sequenceId) async {
    final instance = await StorageService.getSelectedInstanceUrl();
    final url = "$instance/sequence/${sequenceId}";
    await Share.share(url,
        subject: AppLocalizations.of(context)!.titleShareUrl);
  }
}
