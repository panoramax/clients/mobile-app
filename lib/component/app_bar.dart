part of panoramax;

PreferredSizeWidget PanoramaxAppBar(
    {context, title = "Panoramax", backEnabled = true}) {
  return AppBar(
    title: AtomicTitleText.large(text: title, color: Theme.of(context).appBarTheme.titleTextStyle?.color),
    automaticallyImplyLeading: backEnabled,
  );
}
