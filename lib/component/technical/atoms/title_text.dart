import 'package:flutter/material.dart';

enum EnumSize {
  SMALL, MEDIUM, LARGE
}

class AtomicTitleText extends StatelessWidget {
  final String _text;
  final EnumSize _size;
  final IconData? _icon;
  final Color? _color;
  final MainAxisAlignment _horizontalPosition;

  const AtomicTitleText.small({Key? key, required String text, EnumSize size = EnumSize.SMALL, IconData? icon, Color? color}) : _horizontalPosition = MainAxisAlignment.start, _color = color, _icon = icon, _size = size, _text = text, super(key: key);
  const AtomicTitleText.medium({Key? key, required String text, EnumSize size = EnumSize.MEDIUM, IconData? icon, Color? color}) : _horizontalPosition =  MainAxisAlignment.start, _color = color, _icon = icon, _size = size, _text = text, super(key: key);
  const AtomicTitleText.large({Key? key, required String text, EnumSize size = EnumSize.LARGE, IconData? icon, Color? color}) : _horizontalPosition =  MainAxisAlignment.start, _color = color, _icon = icon, _size = size, _text = text, super(key: key);
  const AtomicTitleText.smallCenter({Key? key, required String text, EnumSize size = EnumSize.SMALL, IconData? icon, Color? color}) : _horizontalPosition =  MainAxisAlignment.center, _color = color, _icon = icon, _size = size, _text = text, super(key: key);
  const AtomicTitleText.mediumCenter({Key? key, required String text, EnumSize size = EnumSize.MEDIUM, IconData? icon, Color? color}) : _horizontalPosition = MainAxisAlignment.center, _color = color, _icon = icon, _size = size, _text = text, super(key: key);
  const AtomicTitleText.largeCenter({Key? key, required String text, EnumSize size = EnumSize.LARGE, IconData? icon, Color? color}) : _horizontalPosition = MainAxisAlignment.center, _color = color, _icon = icon, _size = size, _text = text, super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = null;
    switch (_size) {
      case EnumSize.LARGE:
        textStyle = Theme.of(context).textTheme.titleLarge;
        break;
      case EnumSize.MEDIUM:
        textStyle = Theme.of(context).textTheme.titleMedium;
        break;
      default:
        textStyle = Theme.of(context).textTheme.titleSmall;
        break;
    }
    return Row(
      mainAxisAlignment: _horizontalPosition,
      children: [
        if (_icon != null) Icon(_icon),
        if (_icon != null) SizedBox(width: 8.0),
        Text(_text, style: textStyle?.copyWith(color: this._color)),
      ],
    );
  }
}
