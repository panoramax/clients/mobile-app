import 'package:flutter/material.dart';

class AtomicIconButton extends StatelessWidget {
  final String tooltip;
  final IconData icon;
  final IconData? selectedIcon;
  final bool? isSelected;
  final VoidCallback? callback;

  const AtomicIconButton.base(
      {super.key,
      required this.tooltip,
      required this.icon,
      this.selectedIcon = null,
      this.isSelected = null,
      this.callback = null});

  const AtomicIconButton.switchAtPressed(
      {super.key,
      required this.tooltip,
      required this.selectedIcon,
      required this.icon,
      required this.isSelected,
      required this.callback});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      padding: EdgeInsets.only(left: 8),
      icon: Icon(this.icon),
      iconSize: 30,
      tooltip: tooltip,
      isSelected: isSelected,
      selectedIcon: Icon(this.selectedIcon,
          color: Theme.of(context).colorScheme.secondary),
      color: Theme.of(context).colorScheme.tertiary,
      onPressed: callback,
    );
  }
}
