import 'package:flutter/material.dart';

var LargeButtonStyle = FilledButton.styleFrom(
  iconSize: 20,
  minimumSize: Size(150, 50),
  textStyle: TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w700
  )
);

class AtomicFilledButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final String? tooltip;
  final VoidCallback? callback;
  final ButtonStyle? style;

  const AtomicFilledButton.base({
    super.key,
    required this.text,
    required this.icon,
    required this.callback,
    this.tooltip,
    this.style
  });

  @override
  Widget build(BuildContext context) {
    final button =  FilledButton.icon(
      icon: Icon(this.icon),
      onPressed: callback,
      label: Text(text),
      style: style
    );
    return tooltip != null ? Tooltip(message: tooltip, child: button) : button;
  }
}
