part of panoramax;

class PictureToUploadThumbnail extends StatefulWidget {
  const PictureToUploadThumbnail({super.key, required this.picture, required this.onValueChanged});
  final SequencePictureDto picture;
  final ValueChanged onValueChanged;

  @override
  State<PictureToUploadThumbnail> createState() => _PictureToUploadThumbnailState();
}

class _PictureToUploadThumbnailState extends State<PictureToUploadThumbnail> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        Container(
          height: 200,
          decoration: BoxDecoration(
            border: Border.all(
                width: 3,
                color: widget.picture.doesImageHaveCorrectExifTags ? Colors.white : errorColor
            )
          ),
          child: Image.file(
            File(widget.picture.localFilePath),
            fit: BoxFit.cover,
          )
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 24.0,
            width: 24.0,
            child: widget.picture.doesImageHaveCorrectExifTags ?
            Checkbox(
              value: widget.picture.isToSend,
              fillColor: WidgetStateProperty.all(Theme.of(context).colorScheme.onPrimaryContainer),
              checkColor: Theme.of(context).colorScheme.primary,
              onChanged: widget.onValueChanged,
            ) :
            Icon(
              Icons.dangerous,
              color: errorColor,
              size: 24,
            ),
          ),
        )
      ],
    );
  }
}
