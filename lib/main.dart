library panoramax;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:badges/badges.dart' as badges;
import 'package:camera/camera.dart';
import 'package:collection/collection.dart';
import 'package:country_flags/country_flags.dart';
import 'package:equatable/equatable.dart';
import 'package:external_path/external_path.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:flutter_exif_plugin/flutter_exif_plugin.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:introduction_screen/introduction_screen.dart' as IntroductionScreen;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:native_exif/native_exif.dart';
import 'package:panoramax_mobile/component/technical/atoms/icon_button.dart';
import 'package:panoramax_mobile/component/technical/atoms/text_button.dart';
import 'package:panoramax_mobile/component/technical/atoms/title_text.dart';
import 'package:panoramax_mobile/environment.dart';
import 'package:panoramax_mobile/service/api/model/geo_visio.dart';
import 'package:panoramax_mobile/service/collection_service.dart';
import 'package:panoramax_mobile/service/onboarding_service.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:screen_brightness/screen_brightness.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:talker_flutter/talker_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wakelock_plus/wakelock_plus.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:device_info_plus/device_info_plus.dart';

import 'objectbox.g.dart';
import 'service/api/api.dart';

part 'component/app_bar.dart';
part 'component/loader.dart';
part 'component/sequence_listitem.dart';
part 'component/picture_to_upload_thumbnail.dart';
part 'constant.dart';
part 'page/splash_screen.dart';
part 'style/theme/button.dart';
part 'style/theme/text.dart';
part 'page/settings/settings_provider.dart';
part 'page/capture_page.dart';
part 'page/collection_upload_page.dart';
part 'page/homepage.dart';
part 'page/instance_page.dart';
part 'page/onboarding_page.dart';
part 'page/settings/setting_page.dart';
part 'service/database/entities.dart';
part 'service/database/objectbox.dart';
part 'service/database/repositories.dart';
part 'service/homepage_service.dart';
part 'service/model/sequence_card_details.dart';
part 'service/permission_helper.dart';
part 'service/routing.dart';
part 'service/import_picture_service.dart';
part 'service/storage.dart';
part 'service/capture_service.dart';
part 'service/orientation_service.dart';
part 'service/logger.dart';

late ObjectBox objectbox;

Future<void> main() async {
  runZonedGuarded(() async {
    FlutterError.onError = (FlutterErrorDetails details) {
      Logger.getInstance().error(details);
    };
    GetIt.instance
        .registerLazySingleton<NavigationService>(() => NavigationService());
    WidgetsFlutterBinding.ensureInitialized();
    await SequenceRepository.initializeDatabase();
    runApp(
        ChangeNotifierProvider<SettingsProvider>(
            create: (_) => SettingsProvider(), child: const PanoramaxApp()
        )
    );
  }, (error, stackTrace) {
    Logger.getInstance().error(error, stackTrace);
  });
}

class PanoramaxApp extends StatefulWidget {
  const PanoramaxApp({Key? key}) : super(key: key);

  @override
  State<PanoramaxApp> createState() => _PanoramaxAppState();

  static void setLocale(BuildContext context, Locale newLocale) {
    StorageService.setLang(newLocale.languageCode);
    _PanoramaxAppState? state =
    context.findAncestorStateOfType<_PanoramaxAppState>();
    state?.setLocale(newLocale);
  }

  static void setTheme(BuildContext context, ThemeMode newTheme) {
    StorageService.setTheme(newTheme.name);
    _PanoramaxAppState? state =
    context.findAncestorStateOfType<_PanoramaxAppState>();
    state?.setTheme(newTheme);
  }
}

class _PanoramaxAppState extends State<PanoramaxApp> {
  Locale? _locale;
  ThemeMode? _theme;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  void setTheme(ThemeMode theme) {
    setState(() {
      _theme = theme;
    });
  }

  _loadLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _languageCode = prefs.getString(currentLangKey);
    if (_languageCode != null) {
      setLocale(Locale(_languageCode));
    }
  }

  _loadTheme() async {
    return setTheme(await StorageService.getTheme());
  }

  _loadSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    context
        .read<SettingsProvider>()
        .changeIsFlash(prefs.getBool(currentIsFlashTurnOnKey) ?? false);
    context.read<SettingsProvider>().changeReduceBrightnessOnCapture(
        prefs.getBool(currentReduceBrightnessOnCaptureKey) ?? false);
    context
        .read<SettingsProvider>()
        .changeSentViaWifiOnly(prefs.getBool(currentIsFlashTurnOnKey) ?? false);
  }

  @override
  void initState() {
    super.initState();
    _loadLang();
    _loadTheme();
    _loadSettings();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Panoramax',
      locale: _locale,
      home: SplashScreen(),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        textTheme: ThemeText.get(),
        primaryColor: DarkPrimaryContainerColor,
        scaffoldBackgroundColor: DarkSurface,
        dropdownMenuTheme: DropdownMenuThemeData(
          menuStyle: MenuStyle(
            backgroundColor:
            WidgetStateProperty.all(DarkPrimaryContainerColor),
            alignment: Alignment.bottomCenter
          )
        ),
        cardTheme: CardTheme(
          color: Colors.black,
          elevation: 3,
          shadowColor: Colors.white10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7.5),
          ),
        ),
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.black,
          titleTextStyle: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 24,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.white70, // Light icon color
        ),
        bottomAppBarTheme: BottomAppBarTheme(
          color: Colors.black, // Dark Bottom Nav
        ),
        canvasColor: DarkPrimaryContainerColor,
        navigationBarTheme: NavigationBarThemeData(
          backgroundColor: Colors.black,
          elevation: 4,
          indicatorColor: DarkPrimaryColor,
          iconTheme: WidgetStateProperty.resolveWith((states) {
            if (states.contains(WidgetState.selected)) {
              return IconThemeData(color: DarkPrimaryContainerColor);
            } else if (states.contains(WidgetState.disabled)) {
              return IconThemeData(color: Colors.white24);
            } else if (states.contains(WidgetState.any)) {
              return IconThemeData(color: Colors.white);
            }
          })
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: DarkPrimaryContainerColor,
          selectedItemColor: Color(0xffcca657),
          unselectedItemColor: Colors.white70,
        ),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: Colors.white70),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide.none,
          ),
        ),
        outlinedButtonTheme: ThemeButton.get(),
        colorScheme: ColorScheme.dark(
          primary: DarkPrimaryColor,
          onPrimary: Colors.white,
          primaryContainer: DarkPrimaryContainerColor,
          onPrimaryContainer: SecondaryColor,
          secondary: DarkSecondaryColor,
          onSecondary: Colors.white,
          secondaryContainer: DarkSecondaryContainerColor,
          onSecondaryContainer: Colors.black,
          surface: DarkSurface,
          onSurface: Colors.white,
          error: Colors.red,
          onError: Colors.white,
          tertiary: DarkSecondaryContainerColor,
          tertiaryContainer: Colors.black,
          brightness: Brightness.dark,
        )
      ),
      theme: ThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: Surface,
        appBarTheme: AppBarTheme(
          foregroundColor: Colors.white,
          shadowColor: Colors.black,
          surfaceTintColor: Colors.transparent,
          elevation: 5,
          backgroundColor: PrimaryColor,
          titleTextStyle: TextStyle(color: Colors.white)
        ),
        bottomAppBarTheme: BottomAppBarTheme(
          color: PrimaryColor,
        ),
        canvasColor: Colors.white,
        navigationBarTheme: NavigationBarThemeData(
          backgroundColor: PrimaryColor,
          elevation: 4,
          indicatorColor: SecondaryColor,
          iconTheme: WidgetStateProperty.resolveWith((states) {
            if (states.contains(WidgetState.selected)) {
              return IconThemeData(color: PrimaryColor);
            } else if (states.contains(WidgetState.disabled)) {
              return IconThemeData(color: Colors.white24);
            } else if (states.contains(WidgetState.any)) {
              return IconThemeData(color: Colors.white);
            }
          })
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: PrimaryColor,
          selectedItemColor: VISTA_BLUE,
          unselectedItemColor: Colors.white70,
        ),
        cardTheme: CardTheme(
          color: lightWhite,
        ),
        colorScheme: ColorScheme.highContrastLight(
          primaryContainer: PrimaryColor,
          onPrimaryContainer: SecondaryColor,
          primary: PrimaryColor,
          onPrimary: Colors.white,
          secondaryContainer: SecondaryColor,
          tertiaryContainer: TertiaryContainerColor,
          secondary: VISTA_BLUE,
          tertiary: Colors.white,
          surface: Surface
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(PrimaryColor),
            foregroundColor: WidgetStateProperty.all(Colors.white),
          )
        ),
        segmentedButtonTheme: SegmentedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.resolveWith<Color>(
                (Set<WidgetState> states) {
              if (states.contains(WidgetState.selected)) {
                return VISTA_BLUE;
              }
              return Colors.white;
            },
          ),
          iconColor: WidgetStateProperty.resolveWith<Color>(
                  (Set<WidgetState> states) {
                if (states.contains(WidgetState.selected)) {
                  return Colors.white;
                }
                return PrimaryColor;
              }),
          foregroundColor: WidgetStateProperty.resolveWith<Color>(
                (Set<WidgetState> states) {
              if (states.contains(WidgetState.selected)) {
                return Colors.white;
              }
              return PrimaryColor;
            },
          ),
        )
      ),
        textTheme: ThemeText.get(),
        outlinedButtonTheme: ThemeButton.get(),
        listTileTheme: ListTileThemeData(
          selectedColor: Colors.white, selectedTileColor: VISTA_BLUE
        ),
        snackBarTheme:
        SnackBarThemeData(backgroundColor: PrimaryContainerColor),
        useMaterial3: true,
      ),
      themeMode: _theme,
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'),
        Locale('fr'),
        Locale('da'),
        Locale('it'),
      ],
      localeResolutionCallback: (deviceLocale, supportedLocales) {
        for (var locale in supportedLocales) {
          if (deviceLocale != null &&
            deviceLocale.languageCode == locale.languageCode) {
            return deviceLocale;
          }
        }
        return supportedLocales.first;
      },
      onGenerateRoute: generateRoutes,
      navigatorObservers: [
        TalkerRouteObserver(Logger.getInstance())
      ],
      navigatorKey: GetIt.instance<NavigationService>().navigatorKey);
    }
}
