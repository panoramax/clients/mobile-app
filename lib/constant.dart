part of panoramax;

const MaterialColor DEFAULT_COLOR = Colors.indigo;
const bool API_IS_HTTPS = false;
const Color BLUE = Color(0xFF010D37);
const Color VISTA_BLUE = Color.fromRGBO(73, 112, 253, 1);

// const Color PrimaryColor = Color.fromRGBO(12, 28, 98, 1.0);
const Color PrimaryColor = Color(0xFF010D37);
const Color PrimaryContainerColor = Color.fromRGBO(12, 28, 98, 1.0);
const Color SecondaryColor = Color.fromRGBO(203, 221, 252, 1);
const Color TertiaryContainerColor = Colors.white60;
const Color errorColor = Colors.redAccent;
const Color Surface = Color.fromRGBO(203, 221, 252, 1);
const Color lightWhite = Color.fromRGBO(239, 239, 239, 1.0);

const Color DarkPrimaryColor = Color(0xffcca657);
const Color DarkPrimaryContainerColor = Color(0xf92f3131);
const Color DarkSecondaryColor = Color(0xffcf982a);
const Color DarkSecondaryContainerColor = Color(0xfffbedc3);
const Color DarkSurface = Color(0xf9717171);

DateFormat dateFormat = DateFormat("d MMM yy, H:m");

enum CaptureButtonState {
  SINGLE_PHOTO_AVAILABLE,
  SEQUENCE_PHOTO_AVAILABLE,
  SINGLE_PHOTO_IN_PROGRESS,
  SEQUENCE_PHOTO_IN_PROGRESS,
  GPS_NOT_GRANTED,
  GPS_IN_ACQUISITION,
  GPS_NOT_PRECISE
}

enum CaptureMode {
  SINGLE,
  BURST
}

const double GPS_ACCURACY_THRESHOLD = 20;
