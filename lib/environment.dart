import 'package:flutter/widgets.dart';
import 'package:panoramax_mobile/service/model/managed_instance.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

final class Environment {
  static List<ManagedInstance> getDefaultManagedInstances(BuildContext context) {
    return [
      ManagedInstance(
        key: "ign",
        hostname: "panoramax.ign.fr",
        title: AppLocalizations.of(context)!.instanceIgnTitle,
        geographicCoverageTitle: AppLocalizations.of(context)!.ignGeographicCoverageTitle,
        geographicCoverageDescription: AppLocalizations.of(context)!.ignGeographicCoverageDescription,
        licenceTitle: AppLocalizations.of(context)!.ignLicenceTitle,
        licenceDescription: AppLocalizations.of(context)!.ignLicenceDescription
      ),
      ManagedInstance(
          key: "osm",
          hostname: "panoramax.openstreetmap.fr",
          title: AppLocalizations.of(context)!.instanceOsmTitle,
          geographicCoverageTitle: AppLocalizations.of(context)!.osmGeographicCoverageTitle,
          geographicCoverageDescription: AppLocalizations.of(context)!.osmGeographicCoverageDescription,
          licenceTitle: AppLocalizations.of(context)!.osmLicenceTitle,
          licenceDescription: AppLocalizations.of(context)!.osmLicenceDescription
      )
    ];
  }

  static const rawManagedInstancesFromEnv = String.fromEnvironment('PANORAMAX_MANAGED_INSTANCES');
  static var managedInstancesFromEnv = parseManagedInstanceList(rawManagedInstancesFromEnv);
}