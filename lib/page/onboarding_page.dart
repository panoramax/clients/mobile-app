part of panoramax;

const defaultSpaceBetweenText = EdgeInsets.all(8.0);

class OnboardingPage extends StatefulWidget {

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: GlobalKey<ScaffoldState>(),
      backgroundColor: BLUE,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: IntroductionScreen.IntroductionScreen(
                globalHeader: this.header(context),
                pages: [
                  this.firstPage(context),
                  this.secondPage(context),
                  this.thirdPage(context),
                  // this.fourthPage(context), // Uncomment when the application displays the map
                  this.fifthPage(context)
                ],
                showNextButton: true,
                showSkipButton: true,
                showDoneButton: true,
                skip: Text(AppLocalizations.of(context)!.onBoardingSkipButton),
                next: Text(AppLocalizations.of(context)!.onBoardingNextButton),
                done: Text(AppLocalizations.of(context)!.onBoardingCompleteButton),
                skipStyle: ElevatedButton.styleFrom(
                  foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer,
                  textStyle: TextStyle(
                      color: SecondaryColor
                  )
                ),
                nextStyle: FilledButton.styleFrom(
                  backgroundColor: SecondaryColor,
                  foregroundColor: BLUE,
                ),
                doneStyle: FilledButton.styleFrom(
                  backgroundColor: SecondaryColor,
                  foregroundColor: BLUE,
                ),
                globalBackgroundColor: PrimaryColor,
                bodyPadding: EdgeInsets.fromLTRB(0, 115, 0, 0),
                controlsPadding: EdgeInsets.fromLTRB(15, 0, 15, 25),
                controlsPosition: IntroductionScreen.Position(left: 0, right: 0, bottom: 0),
                dotsDecorator: IntroductionScreen.DotsDecorator(
                  color: Colors.grey,
                  activeColor: Theme.of(context).colorScheme.onPrimaryContainer,
                ),
                dotsContainerDecorator: BoxDecoration(
                  color: BLUE
                ),
                resizeToAvoidBottomInset: true,
                onDone: OnboardingService.terminateOnboarding,
                controlsMargin: EdgeInsets.zero
              ),
            )
          ],
        ),
      ),
    );
  }

  Container header(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            BLUE,
            BLUE,
          ],
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            "assets/logo-mobile-app-beta.png",
            width: 75,
          ),
          Text(
            "Panoramax",
            style: Theme.of(context).textTheme.displaySmall!.apply(
              color: Theme.of(context).colorScheme.onPrimaryContainer
            ),
          )
        ],
      ),
    );
  }

  IntroductionScreen.PageViewModel firstPage(BuildContext context) {
    return IntroductionScreen.PageViewModel(
      decoration: IntroductionScreen.PageDecoration(
        bodyPadding: EdgeInsets.fromLTRB(0, 20, 0, 0),
        pageMargin: EdgeInsets.zero,
        contentMargin: EdgeInsets.zero
      ),
      useScrollView: false,
      titleWidget: Column(
        children: [
          Container(
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen1Title,
              style: Theme.of(context).textTheme.displaySmall!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
      bodyWidget: Container(
        height: 380,
        child: Stack(
          children: [
            new Container(
              alignment: Alignment.center,
              child: Container(
                child: Image.asset(
                  "assets/onboarding/page-1.png",
                  height: 380
                )
              ),
            ),
            new Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(),
                child: Container(
                  padding: EdgeInsets.only(left: 150),
                  height: 75,
                  child: FittedBox(
                    child: FloatingActionButton(
                      shape: const CircleBorder(),
                      backgroundColor: Colors.blue,
                      child: new Icon(Icons.share),
                      onPressed: null
                    ),
                  )
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  IntroductionScreen.PageViewModel secondPage(BuildContext context) {
    return IntroductionScreen.PageViewModel(
      decoration: IntroductionScreen.PageDecoration(
        pageMargin: EdgeInsets.zero,
        contentMargin: EdgeInsets.zero,
        bodyPadding: EdgeInsets.zero
      ),
      useScrollView: false,
      titleWidget: Container(
        height: 280,
        child: Stack(
          children: [
            new Container(
              padding: EdgeInsets.only(right: 50),
              alignment: Alignment.center,
              child: Container(
                child: Image.asset(
                  "assets/onboarding/page-2.png",
                  height: 280,
                  fit: BoxFit.cover,
                  alignment: Alignment.topRight,
                )
              ),
            ),
            new Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.only(),
                child: Container(
                    padding: EdgeInsets.only(left: 250),
                    height: 75,
                    child: FittedBox(
                      child: FloatingActionButton(
                        shape: const CircleBorder(),
                        backgroundColor: Colors.blue,
                        child: new Icon(Icons.send),
                        onPressed: null
                      ),
                    )
                ),
              ),
            )
          ]
        ),
      ),
      bodyWidget: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: defaultSpaceBetweenText,
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen2Title,
              style: Theme.of(context).textTheme.displaySmall!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            padding: defaultSpaceBetweenText,
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen2SubTitle,
              style: Theme.of(context).textTheme.headlineMedium!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  IntroductionScreen.PageViewModel thirdPage(BuildContext context) {
    return IntroductionScreen.PageViewModel(
      decoration: IntroductionScreen.PageDecoration(
        pageMargin: EdgeInsets.zero,
        contentMargin: EdgeInsets.zero,
        bodyPadding: EdgeInsets.zero,
      ),
      useScrollView: false,
      titleWidget: Container(
        padding: defaultSpaceBetweenText,
        child: Text(
          AppLocalizations.of(context)!.onBoardingScreen3Title,
          style: Theme.of(context).textTheme.displaySmall!.apply(
            color: Theme.of(context).colorScheme.onPrimaryContainer,
            fontSizeFactor: 0.85
          ),
          textAlign: TextAlign.center
        ),
      ),
      bodyWidget: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              padding: EdgeInsets.only(top: 40),
              child: Image.asset("assets/onboarding/page-3.png")
          ),
        ],
      ),
    );
  }

  IntroductionScreen.PageViewModel fourthPage(BuildContext context) {
    return IntroductionScreen.PageViewModel(
      decoration: IntroductionScreen.PageDecoration(
          titlePadding: EdgeInsets.symmetric(horizontal: 25, vertical: 0),
          bodyPadding: EdgeInsets.fromLTRB(0, 30, 0, 0),
          pageMargin: EdgeInsets.zero,
          contentMargin: EdgeInsets.zero
      ),
      useScrollView: false,
      titleWidget: Column(
        children: [
          Container(
            padding: defaultSpaceBetweenText,
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen4Title,
              style: Theme.of(context).textTheme.displaySmall!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            padding: defaultSpaceBetweenText,
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen4SubTitle,
              style: Theme.of(context).textTheme.headlineMedium!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
      bodyWidget: Center(
          child: Image.asset("assets/onboarding/page-4.png", height: 380)
      ),
    );
  }

  IntroductionScreen.PageViewModel fifthPage(BuildContext context) {
    return IntroductionScreen.PageViewModel(
      decoration: IntroductionScreen.PageDecoration(
          contentMargin: EdgeInsets.zero
      ),
      titleWidget: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Text(
                AppLocalizations.of(context)!.onBoardingScreen5Title,
                style: Theme.of(context).textTheme.displaySmall!.apply(
                    color: Theme.of(context).colorScheme.onPrimaryContainer
                ),
                textAlign: TextAlign.center
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              AppLocalizations.of(context)!.onBoardingScreen5SubTitle,
              style: Theme.of(context).textTheme.headlineMedium!.apply(
                  color: Theme.of(context).colorScheme.onPrimaryContainer
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
      bodyWidget: Container(
        padding: EdgeInsets.only(top: 50),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          decoration: BoxDecoration(
              color: Colors.white
          ),
          child: Image.asset("assets/onboarding/page-5.png")
        ),
      ),
    );
  }
}
