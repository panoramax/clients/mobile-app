part of panoramax;

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  int _currentPageIndex = 0;
  ThemeMode _darkMode = ThemeMode.system;

  @override
  void initState() {
    super.initState();
    StorageService.getTheme().then((selectedTheme) => {
      setState(() {
        _darkMode = selectedTheme;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PanoramaxAppBar(context: context),
        bottomNavigationBar: NavigationBar(
          onDestinationSelected: (int index) {
            setState(() {
              _currentPageIndex = index;
            });
          },
          selectedIndex: _currentPageIndex,
          destinations: <Widget>[
            NavigationDestination(
              selectedIcon: Icon(Icons.settings),
              icon: Icon(Icons.settings_outlined),
              label: '${AppLocalizations.of(context)!.settings_title}',
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.contact_support),
              icon: Icon(Icons.contact_support_outlined),
              label: '${AppLocalizations.of(context)!.settings_contact}',
              enabled: false,
            ),
          ],
        ),
        body: Consumer<SettingsProvider>(
          builder: (context, provider, child) {
            return SettingsList(
              sections: [
                SettingsSection(
                  title: AtomicTitleText.large(text: AppLocalizations.of(context)!.settings_general),
                  tiles: <AbstractSettingsTile>[
                    CustomSettingsTile(child: languageTile(context)),
                    CustomSettingsTile(child: darkModeTile(context)),
                  ],
                ),
                SettingsSection(
                  title: AtomicTitleText.large(text: AppLocalizations.of(context)!.settings_sensors),
                  tiles: <AbstractSettingsTile>[
                    SettingsTile.switchTile(
                      initialValue: provider.isFlashTurnOn,
                      onToggle: (value) {
                        provider.changeIsFlash(value);
                        StorageService.setIsFlashTurnOn(value);
                      },
                      leading: Icon(Icons.flash_on),
                      title: Text(
                        AppLocalizations.of(context)!.settings_sensors_flash
                      ),
                    ),
                    SettingsTile.switchTile(
                      initialValue: provider.reduceBrightnessOnCapture,
                      onToggle: (value) {
                        provider.changeReduceBrightnessOnCapture(value);
                        StorageService.setReduceBrightnessOnCapture(value);
                      },
                      leading: Icon(Icons.brightness_low),
                      title: Text(AppLocalizations.of(context)!
                          .settings_sensors_brightness),
                    ),
                  ],
                ),
                SettingsSection(
                  title: AtomicTitleText.large(text: AppLocalizations.of(context)!.settings_others),
                  tiles: [
                    SettingsTile.navigation(
                      leading: Icon(Icons.work_history_sharp),
                      title: Text(AppLocalizations.of(context)!.settings_others_logs),
                      onPressed: (context) {
                        GetIt.instance<NavigationService>().pushTo(Routes.logs);
                      },
                    )
                  ]
                )
              ],
            );
          },
        ));
  }

  static const WidgetStateProperty<Icon> thumbIcon =
      WidgetStateProperty<Icon>.fromMap(
    <WidgetStatesConstraint, Icon>{
      WidgetState.selected: Icon(Icons.check),
      WidgetState.any: Icon(Icons.close),
    },
  );

  Widget languageTile(BuildContext context) {
    String selectedLanguage = Localizations.localeOf(context).languageCode;
    Map<String, String> languagesValues = {'en': 'English', 'fr': 'Français', 'da': 'Dansk', 'it': 'Italiano'};

    // List<Locale> supportedLocales = context
    //     .findAncestorWidgetOfExactType<MaterialApp>()!
    //     .supportedLocales
    //     .toList();
    return DropdownButtonFormField<String>(
        alignment: Alignment.bottomCenter,
        decoration: InputDecoration(
            prefix: Row(
              children: [
                SizedBox(width: 25.0),
                Icon(Icons.language),
                SizedBox(width: 25.0),
                Text(
                  AppLocalizations.of(context)!.settings_general_language,
                  style: Theme.of(context).textTheme.titleMedium,
                )
              ],
            ),
            suffixText: languagesValues[selectedLanguage],
            suffixStyle: Theme.of(context).textTheme.labelLarge,
            contentPadding: EdgeInsets.only(right: 25)),
        value: languagesValues[selectedLanguage],
        items: languagesValues.values
            .map((e) => DropdownMenuItem(
                value: e,
                child: Row(children: [
                  CountryFlag.fromLanguageCode(
                      languagesValues.entries
                          .firstWhere((element) => element.value == e)
                          .key,
                      width: 30,
                      height: 30,
                      shape: const RoundedRectangle(6)),
                  SizedBox(width: 25.0),
                  Text(e, style: Theme.of(context).textTheme.labelLarge)
                ])))
            .toList(),
        onChanged: (item) => setState(() {
              MapEntry<String, String> selectedLanguage =
                  languagesValues.entries.firstWhere((x) => item == x.value);
              PanoramaxApp.setLocale(context, Locale(selectedLanguage.key));
            }));
  }

  Widget darkModeTile(BuildContext context) {
    Map<String, String> darkModeValues = {
      ThemeMode.system.name:
          AppLocalizations.of(context)!.settings_general_darkMode_system,
      ThemeMode.light.name:
          AppLocalizations.of(context)!.settings_general_darkMode_light,
      ThemeMode.dark.name:
          AppLocalizations.of(context)!.settings_general_darkMode_dark,
    };

    return DropdownButtonFormField<String>(
        decoration: InputDecoration(
            prefix: Row(
              children: [
                SizedBox(width: 25.0),
                Icon(Icons.dark_mode),
                SizedBox(width: 25.0),
                  AtomicTitleText.medium(
                  text: AppLocalizations.of(context)!.settings_general_darkMode),
              ],
            ),
            suffixText: darkModeValues.entries
                .firstWhereOrNull((element) => element.key == _darkMode.name)
                ?.value,
            suffixStyle: Theme.of(context).textTheme.labelLarge,
            contentPadding: EdgeInsets.only(right: 25)),
        value: _darkMode.name,
        items: darkModeValues.entries
            .map((e) => DropdownMenuItem(
                value: e.key,
                child: Text(e.value,
                    style: Theme.of(context).textTheme.labelLarge)))
            .toList(),
        onChanged: (item) => setState(() {
              _darkMode = ThemeMode.values
                      .firstWhereOrNull((element) => element.name == item) ??
                  ThemeMode.system;
              Logger.getInstance().info("selected mode: ${_darkMode.name}");
              PanoramaxApp.setTheme(context, _darkMode);
            }));
  }
}
