part of panoramax;

class SettingsProvider with ChangeNotifier {
  bool currentIsFlashTurnOn = false;
  bool currentReduceBrightnessOnCapture = false;
  bool currentSentViaWifiOnly = false;

  bool get isFlashTurnOn { return currentIsFlashTurnOn; }
  bool get reduceBrightnessOnCapture { return currentReduceBrightnessOnCapture; }
  bool get sentViaWifiOnly { return currentSentViaWifiOnly; }

  void changeIsFlash(bool flashTurnOn) {
    currentIsFlashTurnOn = flashTurnOn;
    notifyListeners();
  }

  void changeReduceBrightnessOnCapture(bool reduceBrightnessOnCapture) {
    currentReduceBrightnessOnCapture = reduceBrightnessOnCapture;
    notifyListeners();
  }

  void changeSentViaWifiOnly(bool sentViaWifiOnly) {
    currentSentViaWifiOnly = sentViaWifiOnly;
    notifyListeners();
  }
}
