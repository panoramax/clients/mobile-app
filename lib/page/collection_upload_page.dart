part of panoramax;

class CollectionUploadPage extends StatefulWidget {
  const CollectionUploadPage({
    required this.sequenceToCreateId,
    super.key,
    this.sendOnStart = false
  });

  final int sequenceToCreateId;
  // If true, upload collection when widget end initialisation
  final bool sendOnStart;

  @override
  State<StatefulWidget> createState() {
    return CollectionUploadPageState();
  }
}

class CollectionUploadPageState extends State<CollectionUploadPage> {
  late final SequenceDto _sequenceToCreate;
  bool isUploadInProgress = false;
  int numberOfPicturesSent = 0;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _sequenceToCreate = SequenceRepository.getSequence(widget.sequenceToCreateId)!;
    if(widget.sendOnStart) {
      this.uploadSequenceToServer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PanoramaxAppBar(context: context),
        backgroundColor: Theme.of(context).colorScheme.surface,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 30),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: AtomicTitleText.largeCenter(
                    text: CaptureService.getSequenceName(context, _sequenceToCreate.creationDate)
                  ),
                ),
                Expanded(
                  child: ListView.custom(
                    childrenDelegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Center(
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 5),
                            child: PictureToUploadThumbnail(
                              picture: _sequenceToCreate.pictures[index],
                              onValueChanged: (value) {
                                if (!isUploadInProgress) {
                                  setState(() {
                                    _sequenceToCreate.pictures[index].isToSend = value!;
                                  });
                                }
                              },
                            ),
                          ),
                        );
                      },
                      childCount: _sequenceToCreate.pictures.length,
                      addRepaintBoundaries: false,
                      addSemanticIndexes: false,
                    ),
                    shrinkWrap: true,
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: !isUploadInProgress ?
                      Column(
                        children: [
                          if(containsAtLeastOnePictureWithIncorrectExifTags())
                            Padding(
                              padding: const EdgeInsets.only(
                                bottom: 15,
                                left: 12.5,
                                right: 12.5,
                              ),
                              child: Text(
                                AppLocalizations.of(context)!.import_pictures_bad_images_warning,
                                style: Theme.of(context).textTheme.bodyMedium!.apply(
                                  color: errorColor
                                ),
                                textAlign: TextAlign.center
                              ),
                            ),
                          if(containsAtLeastOnePictureWithCorrectExifTags())
                            AtomicFilledButton.base(
                              text: AppLocalizations.of(context)!.newSequenceSendButton,
                              icon: Icons.cloud_upload,
                              callback: () => uploadSequenceToServer(),
                              style: LargeButtonStyle,
                            )
                        ],
                      ) :
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          spacing: 10,
                          children: [
                            Text(
                              AppLocalizations.of(context)!.sendingInProgress,
                                style: Theme.of(context).textTheme.bodyLarge!.apply(
                                color: Theme.of(context).colorScheme.primary
                              ),
                              textAlign: TextAlign.center
                            ),
                            Container(
                              width: 250,
                              child: LinearProgressIndicator(
                                backgroundColor: Colors.white,
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                                minHeight: 5,
                                value: numberOfPicturesSent / _sequenceToCreate.pictures.length,
                              ),
                            ),
                            Text(
                              "${numberOfPicturesSent} / ${_sequenceToCreate.pictures.length}",
                              style: Theme.of(context).textTheme.bodyLarge!.apply(
                                color: Theme.of(context).colorScheme.primary,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      )
                ),
              ],
            ),
          ),
        ),
    );
  }

  bool containsAtLeastOnePictureWithIncorrectExifTags() => _sequenceToCreate.pictures.where((pic) => !pic.doesImageHaveCorrectExifTags).isNotEmpty;
  bool containsAtLeastOnePictureWithCorrectExifTags() => _sequenceToCreate.pictures.where((pic) => pic.doesImageHaveCorrectExifTags).isNotEmpty;

  Future<void> uploadSequenceToServer() async {
    CollectionService.deleteUnselectedPictures(_sequenceToCreate);
    await SequenceRepository.updateSequence(_sequenceToCreate);
    StorageService.getSelectedInstanceUrl().then((instance) async {
      final token = await StorageService.getToken();
      if (instance != null && token != null) {
        setState(() {
          isUploadInProgress = true;
        });
        Stream<int> uploadStream = CollectionService.uploadSequencesToServer(_sequenceToCreate);
        uploadStream.listen(
          (value) {
            setState(() {
              numberOfPicturesSent = value;
            });
          },
          onDone: () => GetIt.instance<NavigationService>().goHome()
        );
      } else {
        GetIt.instance<NavigationService>()
            .pushTo(Routes.instance, arguments: _sequenceToCreate.id);
      }
    });
  }
}
