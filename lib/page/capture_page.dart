part of panoramax;

DateFormat filenameDateFormat = DateFormat("yyyyMMdd_HHmmssSSS");

abstract class ICapturePage {
  Widget createSequenceButton(BuildContext context);

  Widget showSequenceButton(BuildContext context);

  Widget captureButton();

  List<Widget> actionButtons();

  List<Widget> captureModeButtons();
}

class CaptureButton {
  final Icon icon;
  final Color backgroundColor;
  final dynamic onPressedAction;

  CaptureButton(
      {required this.icon,
      required this.backgroundColor,
      required this.onPressedAction});
}

class CapturePage extends StatefulWidget {
  const CapturePage({Key? key, required this.cameras, this.sequenceToEditId})
      : super(key: key);

  final List<CameraDescription>? cameras;
  final int? sequenceToEditId;

  @override
  State<CapturePage> createState() => _CapturePageState();
}

const double minIntervalForGyroscopeRefresh = 200; // in millisecond

class _CapturePageState extends State<CapturePage>
    with WidgetsBindingObserver
    implements ICapturePage {
  final List<AppLifecycleState> _stateHistoryList = <AppLifecycleState>[];
  late CameraController _cameraController;
  bool _isProcessing = false;
  bool _isPermissionDialogOpen = false;
  SequenceDto? _sequenceToCreate;

  int _burstDuration = 1200; //in milliseconds
  bool _isBurstMode = false;
  bool _isBurstPlay = false;
  Timer? _timerBurst;
  CaptureMode captureMode = CaptureMode.SINGLE;

  Stream<Position> _positionStream = Stream.empty();
  StreamSubscription<Position>? _positionStreamSubscription;
  Position? _currentPosition;
  StreamSubscription<CompassEvent>? _currentDirectionStream;
  double? _currentDirection;
  StreamSubscription<GyroscopeEvent>? _gyroscopeStream;
  GyroscopeEvent? _gyroscopeValues;
  double? _gyroscopeCaptureTime;
  double? _gyroscopeDeltaTime;
  final LocationSettings locationSettings =
      LocationSettings(accuracy: LocationAccuracy.high);
  double? _accuracy;

  var isPortraitOrientation = true;

  bool _isFlashTurnOn = false;
  bool _reduceBrightnessOnCapture = false;

  @override
  void dispose() {
    Logger.getInstance().debug("Disposing cameraController");
    stopBurstPictures();
    _cameraController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    WakelockPlus.disable();
    _positionStreamSubscription?.cancel();
    _currentDirectionStream?.cancel();
    _gyroscopeStream?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    if (widget.sequenceToEditId != null) {
      _sequenceToCreate =
          SequenceRepository.getSequence(widget.sequenceToEditId!)!;
    }

    super.initState();

    startLocationUpdates();
    WidgetsBinding.instance.addObserver(this);
    if (WidgetsBinding.instance.lifecycleState != null) {
      _stateHistoryList.add(WidgetsBinding.instance.lifecycleState!);
    }

    _isFlashTurnOn = context.read<SettingsProvider>().isFlashTurnOn;
    _reduceBrightnessOnCapture =
        context.read<SettingsProvider>().reduceBrightnessOnCapture;

    if (widget.cameras?.isNotEmpty ?? false) {
      initCamera(widget.cameras![0]);
    }

    _currentDirectionStream = FlutterCompass.events?.listen((compass) {
      if (compass.heading != null) {
        setState((){
          _currentDirection = (360 + compass.heading!) % 360;
        });
      }
    });

    _gyroscopeStream = gyroscopeEventStream().listen((event) {
      setState((){
        _gyroscopeValues = event;
        double currentTime = DateTime.now().millisecondsSinceEpoch.toDouble();
        if(_gyroscopeCaptureTime != null) {
          var elapsedTime = (currentTime - _gyroscopeCaptureTime!).abs();
          if (minIntervalForGyroscopeRefresh < elapsedTime) {
            _gyroscopeDeltaTime = elapsedTime / 1000.0;
            _gyroscopeCaptureTime = currentTime;
          }
        } else {
          _gyroscopeCaptureTime = currentTime;
        }
      });
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      startLocationUpdates();
    }
  }

  void startLocationUpdates() async {
    //check if GPS is enabled
    if (!await Geolocator.isLocationServiceEnabled()) {
      showGPSDialog();
      return;
    }

    final permission = await Geolocator.checkPermission();
    switch (permission) {
      case LocationPermission.denied:
      case LocationPermission.unableToDetermine:
      case LocationPermission.deniedForever:
        var result = await Geolocator.requestPermission();
        if (result == LocationPermission.deniedForever &&
            !_isPermissionDialogOpen) {
          await showPermissionDialog();
        }
        break;
      case LocationPermission.always:
      case LocationPermission.whileInUse:
        if (_isPermissionDialogOpen) {
          Navigator.of(context).pop();
          _isPermissionDialogOpen = false;
        }
        _positionStream =
            Geolocator.getPositionStream(locationSettings: locationSettings);
        if (_positionStreamSubscription == null) {
          _positionStreamSubscription =
              _positionStream.listen((Position position) {
            setState(() {
              _currentPosition = position;
              _accuracy = position.accuracy;
            });
          });
        }
        break;
    }
  }

  void goToSettings() async {
    await Geolocator.openAppSettings();
  }

  void goToCollectionCreationPage() async {
    await _cameraController.setFlashMode(FlashMode.off);
    GetIt.instance<NavigationService>().pushTo(Routes.newSequenceUpload,
      arguments: {
        'sequenceToCreateId': _sequenceToCreate!.id,
        'sendOnStart': false
      }
    );
  }

  Future takePicture() async {
    var newSequenceToCreate;
    if (_sequenceToCreate == null) {
      newSequenceToCreate = await CaptureService.createSequenceDto(context);
    }
    setState(() {
      _isProcessing = true;
      if (newSequenceToCreate != null) {
        _sequenceToCreate = newSequenceToCreate;
      }
    });
    if (!_cameraController.value.isInitialized) {
      return null;
    }
    if (_cameraController.value.isTakingPicture) {
      return null;
    }
    try {
      Logger.getInstance().info("takePicture requested");
      await Future.wait(
        [
          getPictureFromCamera(),
        ],
      ).then((value) async {
        setState(() {
          _isProcessing = false;
        });
        final XFile rawImage = value[0];
        final Position currentLocation = await Geolocator.getCurrentPosition(
          locationSettings: LocationSettings(accuracy: LocationAccuracy.high)
        );
        Logger.getInstance().debug("Picture taken");
        Logger.getInstance().debug("addExifTags");
        await addExifTags(rawImage, currentLocation);
        Logger.getInstance().debug("ExifTags added");
        Logger.getInstance().debug("Add image local list");
        await addImageToList(rawImage);
        Logger.getInstance().info("Image added local list");
      });
    } on CameraException catch (e) {
      Logger.getInstance().error('Error occurred while taking picture', e);
      return null;
    }
  }

  void takeBurstPictures() async {
    if (_timerBurst != null) {
      Logger.getInstance().info("End burst mode");
      stopBurstPictures();
      await ScreenBrightness.instance.resetApplicationScreenBrightness();
    } else {
      if (_reduceBrightnessOnCapture) {
        ScreenBrightness.instance.setApplicationScreenBrightness(0.05);
      }
      Logger.getInstance().info("Start burst mode");
      startBurstPictures();
    }
    setState(() {
      _isBurstPlay = !_isBurstPlay;
    });
  }

  void stopBurstPictures() {
    if (_timerBurst != null) {
      _timerBurst!.cancel();
      _timerBurst = null;
    }
  }

  Future startBurstPictures() async {
    takePicture();
    _timerBurst = Timer.periodic(Duration(milliseconds: _burstDuration), (timer) {
      takePicture();
    });
  }

  Future<void> addImageToList(XFile rawImage) async {
    final sequenceToCreate =
        await CaptureService.addImageSequence(_sequenceToCreate!, rawImage);
    setState(() {
      _sequenceToCreate = sequenceToCreate;
    });
  }

  Future<XFile> getPictureFromCamera() async {
    Logger.getInstance().debug("_cameraController.takePicture");
    final XFile rawImage = await _cameraController.takePicture();
    Logger.getInstance().debug("StorageService.getImageFolder");
    final panoramaxImageRootFolder = await StorageService.getImageFolder();
    final newPath = path.join(panoramaxImageRootFolder.path, getPictureFilename());
    Logger.getInstance().debug("Saving image as ${newPath}");
    await rawImage.saveTo(newPath);
    Logger.getInstance().debug("Image saved as ${newPath}");
    return XFile(newPath);
  }

  String getPictureFilename() => "PX_${filenameDateFormat.format(DateTime.now())}.jpg";

  Future<void> addExifTags(XFile rawImage, Position currentLocation) async {
    Logger.getInstance().debug("add exif tag : " +
        currentLocation.latitude.toString() +
        " " +
        currentLocation.longitude.toString());

    if (Platform.isIOS) {
      var exif = await Exif.fromPath(rawImage.path);
      await exif.writeAttributes({
        'GPSLatitude': currentLocation.latitude,
        'GPSLongitude': currentLocation.longitude,
        'GPSAltitude': currentLocation.altitude
      });
    } else {
      final exif = FlutterExif.fromPath(rawImage.path);
      await exif.setLatLong(
        currentLocation.latitude,
        currentLocation.longitude
      );
      await exif.setAltitude(currentLocation.altitude);
      if (_currentDirection != null) {
        await exif.setAttribute("GPSImgDirection", "${(_currentDirection! * 100).truncate()}/100");
      }
      await exif.saveAttributes();

      if (_gyroscopeValues != null && _gyroscopeDeltaTime != null) {
        double yaw = 0, pitch = 0, roll = 0;
        yaw += _gyroscopeValues!.z * _gyroscopeDeltaTime! * (180 / pi);
        pitch += _gyroscopeValues!.x * _gyroscopeDeltaTime! * (180 / pi);
        roll += _gyroscopeValues!.y * _gyroscopeDeltaTime! * (180 / pi);
        addXMPToImage(rawImage.path, yaw, pitch, roll);
      }

      await debugPrintImageExifTags(rawImage);
    }
  }

  void addXMPToImage(String imagePath, double yaw, double pitch, double roll) {
    String xmpData = '''
<?xpacket begin="" id="W5M0MpCehiHzreSzNTczkc9d"?>
<x:xmpmeta xmlns:x="adobe:ns:meta/">
  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <rdf:Description rdf:about="" xmlns:Camera="http://pix4d.com/camera/1.0" >
      <Camera:Yaw>${yaw.toStringAsFixed(2)}</Camera:Yaw>
      <Camera:Pitch>${pitch.toStringAsFixed(2)}</Camera:Pitch>
      <Camera:Roll>${roll.toStringAsFixed(2)}</Camera:Roll>
    </rdf:Description>
  </rdf:RDF>
</x:xmpmeta>
<?xpacket end="w"?>
''';

    File imageFile = File(imagePath);
    List<int> imageBytes = imageFile.readAsBytesSync();

    // Add XMP official header
    String xmpHeader = "http://ns.adobe.com/xap/1.0/\u0000";

    // Convert XMP into bytes
    List<int> xmpBytes = utf8.encode(xmpHeader + xmpData);

    List<int> app1Header = [0xFF, 0xE1];
    int length = xmpBytes.length + 2;
    app1Header.add((length >> 8) & 0xFF);
    app1Header.add(length & 0xFF);

    // Add XMP after JPEG Markor (FF D8)
    List<int> modifiedImage = [];
    modifiedImage.addAll(imageBytes.sublist(0, 2));
    modifiedImage.addAll(app1Header);
    modifiedImage.addAll(xmpBytes);
    modifiedImage.addAll(imageBytes.sublist(2));

    File(imagePath).writeAsBytesSync(modifiedImage);
  }

  Future<void> debugPrintImageExifTags(XFile rawImage) async {
    final exif2 = await Exif.fromPath(rawImage.path);
    final attributes = await exif2.getAttributes();
    var GPSImgDirection = await exif2.getAttribute("GPSImgDirection");
    var GPSAltitude = await exif2.getAttribute("GPSAltitude");
    var xmp = await exif2.getAttribute("Xmp");
    Logger.getInstance().info("file: ${rawImage}, all known attributes: ${attributes.toString()}, GPSImgDirection: ${GPSImgDirection}, GPSAltitude: ${GPSAltitude}, xmp: ${xmp}");
  }

  Future initCamera(CameraDescription cameraDescription) async {
    _cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.max,
      enableAudio: false,
    );
    try {
      await _cameraController.initialize().then((_) async {
        await _cameraController.prepareForVideoRecording();
        if (!mounted) return;
        setState(() {});
      });
      await _cameraController.setFocusMode(FocusMode.locked);
      await setCameraFlashMode();
    } on CameraException catch (e) {
      Logger.getInstance().debug("camera error $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    WakelockPlus.enable();
    if (widget.cameras?.isEmpty ?? true) {
      return Scaffold(
          key: GlobalKey<ScaffoldState>(),
          body: Center(
            child: Text(AppLocalizations.of(context)!.noCameraFoundError),
          ));
    }
    return PopScope(
      onPopInvokedWithResult: (didPop, result) {
        OrientationService.allowOnlyPortrait();
      },
      child: OrientationBuilder(
        builder: (context, orientation) {
          isPortraitOrientation = orientation == Orientation.portrait;
          return Stack(children: [
            Positioned.fill(
              child: Container(
                color: Theme.of(context)
                  .colorScheme
                  .primaryContainer,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: isPortraitOrientation ? 60 : 0, bottom: isPortraitOrientation ? 170 : 0),
              child: cameraPreview()
            ),
            if (!CaptureService.isGpsAccuracyGood(_accuracy))
              accurancyComponent(),
            // FIXME SSE manage only one layout with icon, text and component rotate
            (!isPortraitOrientation)
                ? landscapeLayout(context)
                : portraitLayout(context),
            if (_isProcessing && !_isBurstMode) processingLoader(context),
            if (!CaptureService.isGpsAccuracyGood(_accuracy))
              alertDialogGpsAccurency(),
          ]);
        },
      ),
    );
  }

  Widget accurancyComponent() {
    return _accuracy == null
        ? Container()
        : Padding(
            padding: EdgeInsets.only(left: 32, top: 10),
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                padding: EdgeInsets.all(8.0),
                child: Row(mainAxisSize: MainAxisSize.min, children: [
                  Icon(
                    !CaptureService.isGpsAccuracyGood(_accuracy)
                        ? Icons.error
                        : Icons.gps_fixed,
                    size: 24,
                    color: !CaptureService.isGpsAccuracyGood(_accuracy)
                        ? Colors.red
                        : Theme.of(context).colorScheme.primary,
                  ),
                  SizedBox(width: 8),
                  DefaultTextStyle(
                    style: TextStyle(
                        color: !CaptureService.isGpsAccuracyGood(_accuracy)
                            ? Colors.red
                            : Theme.of(context).colorScheme.secondary),
                    child: Text(AppLocalizations.of(context)!
                        .meters(_accuracy?.toInt() as Object)),
                  )
                ])));
  }

  Widget portraitLayout(BuildContext context) {
    var actionBar = Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: actionButtons());
    var captureModesBar = Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: captureModeButtons()),
    );
    return Stack(
      children: [
        _isBurstMode
            ? BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(color: Colors.transparent),
              )
            : Container(),
        Center(
          child: _isBurstMode ? askTurnDevice() : Container(),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
              mainAxisSize: MainAxisSize.min,
              spacing: 20.0,
              children: [actionBar, captureModesBar, subActionButtons()]),
        ),
      ],
    );
  }

  List<Widget> defaultBurstBar() {
    return [
      Container(height: !isPortraitOrientation ? 80 : 0),
      (!_isBurstMode || !isPortraitOrientation) ? captureButton() : Container(),
      Container(
        height: 70,
      ),
    ];
  }

  List<Widget> fullBurstBar() {
    return [
      badges.Badge(
          position: badges.BadgePosition.bottomEnd(),
          badgeStyle: badges.BadgeStyle(
              badgeColor: Theme.of(context).colorScheme.secondary),
          badgeContent: Text('${_sequenceToCreate!.pictures.length}'),
          child: showSequenceButton(context)),
      (!_isBurstMode || !isPortraitOrientation) ? captureButton() : Container(),
      createSequenceButton(context)
    ];
  }

  Widget askTurnDevice() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.screen_rotation, size: 120.0, color: Colors.white),
          Padding(
              padding: EdgeInsets.all(50),
              child: Card(
                  child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Row(children: [
                        Icon(
                          Icons.info_outline,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(left: 8),
                                child: Text(
                                    AppLocalizations.of(context)!
                                        .switchCameraRequired,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      fontSize: 16,
                                    )))),
                      ]))))
        ]);
  }

  Widget landscapeLayout(BuildContext context) {
    var actionBar = Container(
        padding: EdgeInsets.only(bottom: 24, top: 24, left: 24, right: 58),
        width: MediaQuery.of(context).size.width,
        child: Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: actionButtons(),
        )));
    Widget createBurstBar() {
      return Container(
          padding: EdgeInsets.all(24),
          height: MediaQuery.of(context).size.height,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: captureModeButtons(),
          ));
    }

    return Container(
        margin: EdgeInsets.only(left: 30, right: 30),
        width: MediaQuery.of(context).size.width,
        child: Stack(children: [createBurstBar(), actionBar, subActionButtons()]));
  }

  bool picturesListIsNotEmpty() =>
      _sequenceToCreate != null && _sequenceToCreate!.pictures.isNotEmpty;

  StatelessWidget cameraPreview() {
    if (_cameraController.value.isInitialized) {
      return Container(
          alignment: Alignment.center, child: CameraPreview(_cameraController));
    } else {
      return Container(
        alignment: Alignment.center,
        color: Colors.transparent,
        child: const Center(child: CircularProgressIndicator()),
      );
    }
  }

  Positioned processingLoader(BuildContext context) {
    return Positioned(
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      child: Loader(
        message: DefaultTextStyle(
          style: Theme.of(context).textTheme.bodyLarge!,
          child: Container(),
        )
      ),
    );
  }

  Widget alertDialogGpsAccurency() {
    return AlertDialog(
      title: Text(AppLocalizations.of(context)!.lowGpsAccuracyTitle),
      content: Text(AppLocalizations.of(context)!.lowGpsAccuracyDesc),
      backgroundColor: Theme.of(context).colorScheme.primary,
      titleTextStyle: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
      contentTextStyle:
          TextStyle(color: Theme.of(context).colorScheme.onPrimary),
    );
  }

  Future<void> showGPSDialog() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.gpsIsDisableTitle),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(AppLocalizations.of(context)!.gpsIsDisableContent),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.common_close),
              onPressed: () {
                startLocationUpdates;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showPermissionDialog() async {
    _isPermissionDialogOpen = true;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.common_permissionDenied),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(AppLocalizations.of(context)!.permissionLocationRequired),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.common_close),
              onPressed: () {
                Navigator.of(context).pop();
                _isPermissionDialogOpen = false;
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.goToSettings),
              onPressed: () {
                goToSettings();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget createSequenceButton(BuildContext context) {
    return IconButton(
        padding: EdgeInsets.zero,
        iconSize: 30,
        icon: const Icon(Icons.send_outlined, color: Colors.white),
        onPressed: goToCollectionCreationPage,
        tooltip:
            AppLocalizations.of(context)!.createSequenceWithPicture_tooltip);
  }

  @override
  Widget showSequenceButton(BuildContext context) {
    return IconButton(
        padding: EdgeInsets.only(left: !isPortraitOrientation ? 10.0 : 0),
        onPressed: goToCollectionCreationPage,
        icon: Wrap(
          spacing: -55,
          children: _sequenceToCreate!.pictures
              .map((photo) {
                return Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).colorScheme.onPrimary,
                          width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      image: picturesListIsNotEmpty()
                          ? DecorationImage(
                              image: FileImage(File(photo.localFilePath)),
                              fit: BoxFit.cover)
                          : null),
                );
              })
              .skip(_sequenceToCreate!.pictures.length > 3
                  ? _sequenceToCreate!.pictures.length - 3
                  : 0)
              .toList(),
        ));
  }

  CaptureButton gpsNotGrantedButton() {
    return CaptureButton(
        icon: Icon(
          Icons.gps_off,
          color: Colors.red,
          size: 35,
        ),
        backgroundColor: Colors.grey,
        onPressedAction: startLocationUpdates);
  }

  CaptureButton gpsInAcquisitionButton() {
    return CaptureButton(
        icon: Icon(
          Icons.not_listed_location,
          color: Colors.red,
          size: 35,
        ),
        backgroundColor: Colors.grey,
        onPressedAction: startLocationUpdates);
  }

  CaptureButton gpsNotPreciseButton() {
    return CaptureButton(
        icon: Icon(
          Icons.gps_fixed,
          color: Colors.red,
          size: 35,
        ),
        backgroundColor: Colors.grey,
        onPressedAction: startLocationUpdates);
  }

  CaptureButton sequenceInProgressButton() {
    return CaptureButton(
        icon: Icon(
          Icons.stop,
          color: Colors.white,
          size: 40,
        ),
        backgroundColor: Colors.red,
        onPressedAction: takeBurstPictures);
  }

  CaptureButton sequenceAvailableButton() {
    return CaptureButton(
        icon: Icon(
          Icons.circle,
          // FIXME add #1b4d71 to theme and apply it
          color: Colors.white,
          size: 50,
        ),
        backgroundColor: Theme.of(context).colorScheme.secondary,
        onPressedAction: takeBurstPictures);
  }

  CaptureButton singlePhotoInProgressButton() {
    return CaptureButton(
        icon: Icon(
          Icons.circle,
          color: Colors.grey,
          size: 50,
        ),
        backgroundColor: Colors.grey,
        onPressedAction: takePicture);
  }

  CaptureButton singlePhotoAvailableButton() {
    return CaptureButton(
        icon: Icon(
          Icons.circle,
          color: Theme.of(context).colorScheme.secondary,
          size: 50,
        ),
        backgroundColor: Colors.white,
        onPressedAction: takePicture);
  }

  CaptureButton getCaptureButton(CaptureButtonState state) {
    return switch (state) {
      CaptureButtonState.SINGLE_PHOTO_AVAILABLE =>
        this.singlePhotoAvailableButton(),
      CaptureButtonState.SEQUENCE_PHOTO_AVAILABLE =>
        this.sequenceAvailableButton(),
      CaptureButtonState.SINGLE_PHOTO_IN_PROGRESS =>
        this.singlePhotoInProgressButton(),
      CaptureButtonState.SEQUENCE_PHOTO_IN_PROGRESS =>
        this.sequenceInProgressButton(),
      CaptureButtonState.GPS_NOT_GRANTED => this.gpsNotGrantedButton(),
      CaptureButtonState.GPS_IN_ACQUISITION => this.gpsInAcquisitionButton(),
      CaptureButtonState.GPS_NOT_PRECISE => this.gpsNotPreciseButton(),
    };
  }

  @override
  Widget captureButton() {
    CaptureButtonState captureButtonState =
        CaptureService.getCaptureButtonState(_currentPosition, _accuracy,
            _isBurstMode, _isBurstPlay, _isProcessing);
    CaptureButton captureButton = getCaptureButton(captureButtonState);
    return GestureDetector(
      child: IconButton(
          onPressed: captureButton.onPressedAction,
          iconSize: 100,
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          icon: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: captureButton.backgroundColor),
            child: captureButton.icon,
          ),
          tooltip: AppLocalizations.of(context)!.capture_tooltip),
    );
  }

  @override
  List<Widget> actionButtons() {
    return picturesListIsNotEmpty() ? fullBurstBar() : defaultBurstBar();
  }

  Widget subActionButtons() {
    return Padding(padding: EdgeInsets.only(bottom: 8), child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      AtomicIconButton.switchAtPressed(
        tooltip: AppLocalizations.of(context)!.settings_sensors_brightness,
        selectedIcon: Icons.flash_on,
        icon: Icons.flash_off,
        isSelected: _isFlashTurnOn,
        callback: () async {
          setState(() {
            _isFlashTurnOn = !_isFlashTurnOn;
          });
          await setCameraFlashMode();
        },
      ),

      // Second AtomicIconButton widget
      AtomicIconButton.switchAtPressed(
        tooltip: AppLocalizations.of(context)!.settings_sensors_flash,
        selectedIcon: Icons.brightness_low_sharp,
        icon: Icons.brightness_high,
        isSelected: _reduceBrightnessOnCapture,
        callback: () async {
          setState(() {
            _reduceBrightnessOnCapture = !_reduceBrightnessOnCapture;
          });
        },
      ),
    ]));
  }

  Future<void> setCameraFlashMode() async {
    await _cameraController.setFlashMode(
      _isFlashTurnOn ? FlashMode.torch : FlashMode.off,
    );
  }

  @override
  List<Widget> captureModeButtons() {
    return [
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
            width: 300,
            child: SegmentedButton<CaptureMode>(
              segments: <ButtonSegment<CaptureMode>>[
                ButtonSegment<CaptureMode>(
                    value: CaptureMode.SINGLE,
                    label:
                        Text(AppLocalizations.of(context)!.photo.toUpperCase()),
                    icon: Icon(Icons.insert_photo),
                    tooltip: AppLocalizations.of(context)!
                        .singleModeCapture_tooltip),
                ButtonSegment<CaptureMode>(
                    value: CaptureMode.BURST,
                    label: Text(
                        AppLocalizations.of(context)!.sequence.toUpperCase()),
                    icon: Icon(Icons.burst_mode),
                    tooltip:
                        AppLocalizations.of(context)!.burstModeCapture_tooltip)
              ],
              selected: <CaptureMode>{captureMode},
              onSelectionChanged: (Set<CaptureMode> newSelection) {
                setState(() {
                  captureMode = newSelection.first;
                  _isBurstMode = captureMode == CaptureMode.BURST;
                  Logger.getInstance().info("Selected mode: ${captureMode}");
                  if (captureMode == CaptureMode.SINGLE) {
                    stopBurstPictures();
                  }
                });
              },
            )),
      ])),
    ];
  }
}
