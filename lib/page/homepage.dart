part of panoramax;

class UserSequenceResponse {
  final bool onlineRetrieveHaveFailed;
  late final List<SequenceCardDetails> items;

  UserSequenceResponse({this.onlineRetrieveHaveFailed = false, required this.items});
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  GlobalKey _refreshKey = GlobalKey();

  @override
  void initState() {
    Logger.getInstance().info("homepage");
    OrientationService.allowOnlyPortrait();
    super.initState();
    final allSequences = SequenceRepository.getAllSequences();
    Logger.getInstance().info("Number of sequences: ${allSequences.length}");
    // Logger.getInstance().debug("allSequences: ${allSequences}");
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PanoramaxAppBar(context: context),
      body: FutureBuilder<bool>(
        future: isInstanceSelected(context),
        builder: (BuildContext context,
          AsyncSnapshot<bool> isInstanceSelectedSnapshot) {
          return FutureBuilder<UserSequenceResponse>(
            future: _refreshUserCollections(
                isInstanceSelectedSnapshot.data
            ),
            builder: (BuildContext context,
                AsyncSnapshot<UserSequenceResponse>
                sequenceListSnapshot) {
              return RefreshIndicator(
                  displacement: 250,
                  strokeWidth: 3,
                  triggerMode: RefreshIndicatorTriggerMode.anywhere,
                  onRefresh: _refresh,
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(16),
                          child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Semantics(
                                  header: true,
                                  child: AtomicTitleText.large(
                                      text:
                                      AppLocalizations.of(context)!
                                          .mySequences),
                                )
                              ])),
                      Expanded(
                        child: () {
                          if (isInstanceSelectedSnapshot.hasData &&
                              sequenceListSnapshot.hasData &&
                              isInstanceSelectedSnapshot
                                  .connectionState ==
                                  ConnectionState.done &&
                              sequenceListSnapshot.connectionState ==
                                  ConnectionState.done) {
                            if (sequenceListSnapshot.data!.items.isNotEmpty) {
                              return Column(
                                children: [
                                  if(sequenceListSnapshot.data!.onlineRetrieveHaveFailed)
                                    Container(
                                        padding: EdgeInsets.all(12),
                                        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                        decoration: BoxDecoration(
                                          color: Colors.red.shade100,
                                          borderRadius: BorderRadius.circular(8),
                                          border: Border.all(color: Colors.red.shade700, width: 1),
                                        ),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Icon(Icons.error, color: Colors.red.shade700, size: 28),
                                            SizedBox(width: 12),
                                            Expanded(
                                              child: Text(
                                                AppLocalizations.of(context)!.remoteCollectionRetrieveError,
                                                style: TextStyle(color: Colors.red.shade900, fontSize: 16),
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                  Expanded(
                                    child: SequencesListView(
                                        sequenceCardDetailsList:
                                        sequenceListSnapshot.data!.items
                                    ),
                                  )
                                ],
                              );
                            } else {
                              return const NoElementView();
                            }
                          } else if (isInstanceSelectedSnapshot
                              .hasError) {
                            Logger.getInstance().error(
                              "Error during sequence retrieve",
                              isInstanceSelectedSnapshot.error,
                              isInstanceSelectedSnapshot.stackTrace
                            );
                            return UnknownErrorView(refreshCallback: () => _refresh());
                          } else if (sequenceListSnapshot.hasError) {
                            Logger.getInstance().error(
                              "Error during sequence retrieve",
                              sequenceListSnapshot.error,
                              sequenceListSnapshot.stackTrace
                            );
                            return UnknownErrorView(refreshCallback: () => _refresh());
                          } else {
                            return const LoaderIndicatorView();
                          }
                        }(),
                      ),
                    ],
                  ));
            });
        }),
      key: GlobalKey<ScaffoldState>(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            HomepageService.goToCapture().then((value) => _refresh());
          },
          child: Icon(
            Icons.add_a_photo_rounded,
            size: 30,
            color: Theme.of(context).colorScheme.primaryContainer,
          ),
          shape: CircleBorder(),
          backgroundColor: Theme.of(context).colorScheme.tertiary,
          foregroundColor: Theme.of(context).colorScheme.onTertiary,
          tooltip: AppLocalizations.of(context)!.createSequence_tooltip),
      bottomNavigationBar: Container(
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(color: Colors.black54, blurRadius: 10)]),
          child: BottomAppBar(
              shadowColor: Colors.black,
              surfaceTintColor: Colors.transparent,
              elevation: 10,
              height: 50,
              shape: const CircularNotchedRectangle(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    padding: EdgeInsets.only(),
                    iconSize: 30,
                    icon: const Icon(Icons.upload_file, color: Colors.white),
                    onPressed: () =>
                        ImportPictureService.importPictures(context),
                    tooltip: AppLocalizations.of(context)!.import_pictures,
                  ),

                  IconButton(
                      padding: EdgeInsets.only(),
                      iconSize: 30,
                      icon: const Icon(Icons.settings, color: Colors.white),
                      onPressed: _goToSettings,
                      tooltip: AppLocalizations.of(context)!.settings_title)
                ],
              ))),
    );
  }

  Future<bool> isInstanceSelected(BuildContext context) async {
    if (!(await OnboardingService.isOnboardingAlreadyDone())) {
      _goToOnboarding();
      return false;
    }
    final instance = await StorageService.getSelectedInstanceUrl();
    return instance != null;
  }

  Future<void> _goToOnboarding() async {
    GetIt.instance<NavigationService>().pushReplacementTo(Routes.onboarding);
  }

  Future<void> _goToSettings() async {
    GetIt.instance<NavigationService>().pushTo(Routes.settings);
  }

  Future<UserSequenceResponse> _refreshUserCollections(bool? isInstanceSelected) async {
    List<GeoVisioLink> onlineUserCollection = [];
    List<SequenceDto> localeUserCollection = SequenceRepository.getAllSequences();
    bool onlineRetrieveHaveFailed = false;
    try {
      onlineUserCollection = isInstanceSelected != null && isInstanceSelected
                  ? (await CollectionService.getUserCollections())?.links ?? []
                  : [];
    } catch (e) {
      onlineRetrieveHaveFailed = true;
      throwIf(localeUserCollection.isEmpty, e);
    } finally {
      return UserSequenceResponse(
        onlineRetrieveHaveFailed: onlineRetrieveHaveFailed,
        items: HomepageService.buildListOfSequenceCardsDetails(
            onlineUserCollection,
            localeUserCollection
        )
      );
    }
  }

  Future<void> _refresh() async {
    if (mounted) {
      _refreshKey = GlobalKey();
      setState(() {});
    }
  }
}

class LoaderIndicatorView extends StatelessWidget {
  const LoaderIndicatorView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Loader(
            message: Text(AppLocalizations.of(context)!.loading),
          ),
        ),
      ],
    );
  }
}

class SequencesListView extends StatelessWidget {
  const SequencesListView({super.key, required this.sequenceCardDetailsList});

  final List<SequenceCardDetails> sequenceCardDetailsList;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom: 25),
      itemCount: sequenceCardDetailsList.length,
      physics:
          const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemBuilder: (BuildContext context, int index) {
        return SequenceListItem(sequenceDetails: sequenceCardDetailsList[index], playAnimationOnStart: index == 0);
      },
    );
  }
}

class NoElementView extends StatelessWidget {
  const NoElementView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20),
      child: Column(
        spacing: 5,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(AppLocalizations.of(context)!.emptyTitle,
              style: Theme.of(context)
                  .textTheme
                  .titleMedium!
                  .apply(fontSizeFactor: 1.3, fontWeightDelta: 1)),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(AppLocalizations.of(context)!.emptySubTitle1,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .apply(fontSizeFactor: 1.2, fontWeightDelta: 1)),
          ),
          Text(AppLocalizations.of(context)!.emptyBody1,
              textAlign: TextAlign.justify,
              softWrap: true,
              style: Theme.of(context).textTheme.bodyMedium!.apply(
                    fontSizeFactor: 1.1,
                  )),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(AppLocalizations.of(context)!.emptySubTitle2,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .apply(fontSizeFactor: 1.2, fontWeightDelta: 1)),
          ),
          Text(AppLocalizations.of(context)!.emptyBody2,
              textAlign: TextAlign.justify,
              style: Theme.of(context).textTheme.bodyMedium!.apply(
                    fontSizeFactor: 1.1,
                  ))
        ],
      ),
    );
  }
}

class UnknownErrorView extends StatelessWidget {
  final VoidCallback? refreshCallback;

  const UnknownErrorView({
    super.key,
    required this.refreshCallback,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppLocalizations.of(context)!.unknownError,
              style: GoogleFonts.nunito(
                  fontSize: 20, color: Colors.red, fontWeight: FontWeight.w400
              ),
            ),
            AtomicFilledButton.base(
                text: AppLocalizations.of(context)!.retry,
                icon: Icons.refresh_rounded,
                callback: refreshCallback
            ),
          ],
        )
      ],
    );
  }
}
