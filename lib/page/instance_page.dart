part of panoramax;

class InstancePage extends StatefulWidget {
  const InstancePage({required this.sequenceToCreateId, super.key});

  final int sequenceToCreateId;

  @override
  State<StatefulWidget> createState() {
    return _InstanceState();
  }
}

class _InstanceState extends State<InstancePage> {
  String? webViewUrl;
  bool isInstanceChosen = false;
  final cookieManager = WebviewCookieManager();
  late final SequenceDto _sequenceToCreate;
  String? _instanceUrl;

  @override
  initState() {
    super.initState();
    _sequenceToCreate = SequenceRepository.getSequence(widget.sequenceToCreateId)!;
  }

  void authenticationRequest(String instanceUrl) {
    setState(() {
      _instanceUrl = instanceUrl;
      webViewUrl = "https://$instanceUrl/api/auth/login?next_url=https%3A%2F%2F$instanceUrl%2F";
      isInstanceChosen = true;
    });
    CollectionService.uploadSequencesToServer(_sequenceToCreate);
  }

  void getJWTToken() async {
    final cookies = await cookieManager.getCookies("https://$_instanceUrl");

    var tokens = await AuthenticationApi.INSTANCE.apiTokensGet(_instanceUrl!, cookies);
    var token =
        await AuthenticationApi.INSTANCE.apiTokenGet(_instanceUrl!, tokens.id, cookies);

    StorageService.setToken(token.jwt_token);
    StorageService.setSelectedInstance(_instanceUrl!);

    Logger.getInstance().info("Connected to instance:${_instanceUrl}");

    GetIt.instance<NavigationService>().pushReplacementTo(
        Routes.newSequenceUpload,
        arguments: {
          'sequenceToCreateId': _sequenceToCreate.id,
          'sendOnStart': true
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: GlobalKey<ScaffoldState>(),
        appBar: PanoramaxAppBar(context: context),
        backgroundColor: !isInstanceChosen ? Theme.of(context).colorScheme.surface: Colors.white,
        body: isInstanceChosen
          ? WebViewWidget(
            controller: WebViewController()
              ..setJavaScriptMode(JavaScriptMode.unrestricted)
              ..setNavigationDelegate(NavigationDelegate(
                onNavigationRequest: (NavigationRequest request) async {
                  bool shouldNavigate = true;
                  if (_instanceUrl != null) {
                    if (request.url == "https://$_instanceUrl/") {
                      getJWTToken();
                      shouldNavigate = false;
                    }
                  }
                  return shouldNavigate
                    ? NavigationDecision.navigate
                    : NavigationDecision.prevent;
                },
              ))
              ..loadRequest(Uri.parse(webViewUrl!)))
          : SingleChildScrollView(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: EdgeInsets.only(bottom: 16),
                      child:
                      AtomicTitleText.large(text: AppLocalizations.of(context)!.instanceChoice_title),
                  ),
                  ...buildManagedInstancesCards(context),
                ],
              )));
  }

  List<Widget> buildManagedInstancesCards(BuildContext context) {
    Logger.getInstance().debug("available instances:${Environment.rawManagedInstancesFromEnv}");
    var managedInstance = Environment.managedInstancesFromEnv.isNotEmpty ?
          Environment.managedInstancesFromEnv:
          Environment.getDefaultManagedInstances(context);
    return managedInstance.map((instance) => ManagedInstancesCard(
        instance.title,
        instance.geographicCoverageTitle,
        instance.geographicCoverageDescription,
        instance.licenceTitle,
        instance.licenceDescription,
        "assets/managedInstance/${instance.key}.png",
        instance.hostname)
    ).toList();
  }

  Widget ManagedInstancesCard(
      String name,
      String geoTitle,
      String geoDescription,
      String licenceTitle,
      String licenceDescription,
      String img,
      String instanceUrl) {
    return Container(
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: DefaultTextStyle(
          style: TextStyle(
            color: Theme.of(context).colorScheme.primary
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: Image.asset(
                  img,
                  height: 150,
                  width: 150,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Divider(),
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Divider(),
                  CustomRowInCard(Icons.public, geoTitle, geoDescription),
                  Divider(),
                  CustomRowInCard(Icons.copyright, licenceTitle, licenceDescription)
                ],
              ),
              Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.fromLTRB(0, 0, 16, 16),
                  child: ElevatedButton.icon(
                    icon: Icon(
                      Icons.cloud_upload,
                      color: Theme.of(context).cardTheme.color
                    ),
                    onPressed: () => authenticationRequest(instanceUrl),
                    label: Text(AppLocalizations.of(context)!.newSequenceSendButton),
                    style: ButtonStyle(
                      backgroundColor: WidgetStateProperty.all(Theme.of(context).colorScheme.primary),
                      foregroundColor: WidgetStateProperty.all(Theme.of(context).cardTheme.color)
                    ),
                  ))
            ],
          )
        ),
      )
    );
  }

  Widget CustomRowInCard(IconData icon, String title, String description) {
    return Container(
      padding: EdgeInsets.all(16),
      alignment: Alignment.topLeft,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(icon),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Column(
              children: [
                Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(description)
              ],
            )
          )
        ],
      )
    );
  }
}
