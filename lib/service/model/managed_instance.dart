import 'dart:convert';

class ManagedInstance {
  ManagedInstance({
    required this.key,
    required this.hostname,
    required this.title,
    required this.geographicCoverageTitle,
    required this.geographicCoverageDescription,
    required this.licenceTitle,
    required this.licenceDescription
  });

  final String key;
  final String hostname;
  final String title;
  final String geographicCoverageTitle;
  final String geographicCoverageDescription;
  final String licenceTitle;
  final String licenceDescription;

  factory ManagedInstance.fromJson(Map<String, dynamic> json) {
    return ManagedInstance(
      key: json['key'],
      hostname: json['hostname'],
      title: json['title'],
      geographicCoverageTitle: json['geographicCoverageTitle'],
      geographicCoverageDescription: json['geographicCoverageDescription'],
      licenceTitle: json['licenceTitle'],
      licenceDescription: json['licenceDescription'],
    );
  }

  @override
  String toString() {
    return 'ManagedInstance{key: $key, hostname: $hostname, title: $title, geographicCoverageTitle: $geographicCoverageTitle, geographicCoverageDescription: $geographicCoverageDescription, licenceTitle: $licenceTitle, licenceDescription: $licenceDescription}';
  }
}

List<ManagedInstance> parseManagedInstanceList(String managedInstanceListJson) {
  if(managedInstanceListJson.isEmpty) {
    return [];
  }
  final List<dynamic> jsonList = json.decode(managedInstanceListJson);
  return jsonList.map((json) => ManagedInstance.fromJson(json)).toList();
}
