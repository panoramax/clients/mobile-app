part of panoramax;

abstract class SequenceCardDetails {
  late String title;
  String? remoteId;
  late int numberOfLocalPictures;
  late int numberOfPublishedPictures;
  late DateTime creationDate;
  late PublicationStatus publicationStatus;
  DateTime? publicationDate;

  Future<Image?> getThumbnail();
  Future<RemoteSequenceStatus> getRemoteStatus();
}

class PublicationIncompleteSequenceCardDetails implements SequenceCardDetails {
  late SequenceDto localDetails;
  @override
  late String title;
  @override
  String? remoteId;
  @override
  late int numberOfLocalPictures;
  @override
  late int numberOfPublishedPictures;
  @override
  late DateTime creationDate;
  @override
  DateTime? publicationDate;
  @override
  PublicationStatus publicationStatus = PublicationStatus.partial;

  Future<Image?> getThumbnail() async {
    return localDetails.pictures.isNotEmpty ? Image.file(
      File(localDetails.pictures[0].localFilePath),
      width: 60,
      height: 60,
      fit: BoxFit.cover
    ): null;
  }

  Future<RemoteSequenceStatus> getRemoteStatus() async {
    return RemoteSequenceStatus.NOT_SENT;
  }
}

class PublicationCompleteSequenceCardDetails implements SequenceCardDetails {
  @override
  String? remoteId;
  @override
  late String title;
  @override
  late int numberOfLocalPictures;
  @override
  late int numberOfPublishedPictures;
  @override
  late DateTime creationDate;
  @override
  late DateTime? publicationDate;
  @override
  PublicationStatus publicationStatus = PublicationStatus.complete;
  late GeoVisioLink remoteDetails;

  Future<Image?> getThumbnail() async {
    final instance = await StorageService.getSelectedInstanceUrl();
    var url = Uri.https(instance!, '/api/collections/${remoteId}/thumb.jpg');
    final token = await StorageService.getToken();

    return Image.network(
      url.toString(),
      headers: <String, String>{
        'Content-Type': 'image/jpeg',
        'Authorization': 'Bearer $token'
      },
      fit: BoxFit.cover
    );
  }

  Future<RemoteSequenceStatus> getRemoteStatus() async {
    if(publicationStatus == PublicationStatus.partial) {
      return RemoteSequenceStatus.PARTIALLY_SENT;
    }

    final remoteImportStatus = await CollectionsApi.INSTANCE
                .getGeovisioStatus(collectionId: remoteId!);
    if(remoteImportStatus.status == "deleted") {
      return RemoteSequenceStatus.DELETED;
    }

    if(remoteImportStatus.status == "hidden") {
      return RemoteSequenceStatus.HIDDEN;
    }

    if(remoteImportStatus.status == "hidden") {
      return RemoteSequenceStatus.HIDDEN;
    }

    if(remoteImportStatus.status == "hidden") {
      return RemoteSequenceStatus.HIDDEN;
    }

    if(remoteImportStatus.items.length != remoteDetails.stats_items?.count) {
      return RemoteSequenceStatus.SENDING;
    }

    if(remoteImportStatus.status == "ready") {
      return RemoteSequenceStatus.READY;
    }

    return RemoteSequenceStatus.BLURRING;
  }
}

enum PublicationStatus {
  complete,
  partial
}

enum RemoteSequenceStatus {
  NOT_SENT,
  PARTIALLY_SENT,
  SENDING,
  BLURRING,
  READY,
  DELETED,
  HIDDEN
}
