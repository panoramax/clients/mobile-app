part of panoramax;

class HomepageService {
  static List<SequenceCardDetails> buildListOfSequenceCardsDetails(List<GeoVisioLink> onlineSequences, List<SequenceDto> localSequences) {
    List<SequenceCardDetails> listOfSequenceCardDetails = [];

    onlineSequences
        .where((sequence) => sequence.rel == "child") // TODO : Pourquoi ce filtre doit être fait ?
        .forEach((sequence) {
          listOfSequenceCardDetails.add(
            PublicationCompleteSequenceCardDetails()
              ..title = sequence.title!
              ..publicationStatus = PublicationStatus.complete
              ..remoteId = sequence.id
              ..numberOfLocalPictures = sequence.stats_items!.count
              ..numberOfPublishedPictures = sequence.stats_items!.count
              ..creationDate = DateTime.parse(sequence.extent!.temporal!.interval![0]![0]!)
              ..remoteDetails = sequence
              ..publicationDate = DateTime.parse(sequence.created!)
          );
        });

    localSequences.forEach((localSequence) {
      var existingOnlineSequence = listOfSequenceCardDetails
          .firstWhereOrNull(
            (onlineSequence) => localSequence.panoramaxSequenceId != null &&
                localSequence.panoramaxSequenceId == onlineSequence.remoteId
          );
      if (existingOnlineSequence != null) {
        if (existingOnlineSequence.numberOfLocalPictures != localSequence.pictures.length) {
          existingOnlineSequence
            ..publicationStatus = PublicationStatus.partial
            ..numberOfLocalPictures = localSequence.pictures.length;
        }
      } else if(localSequence.panoramaxSequenceId == null) {
        listOfSequenceCardDetails.add(
            PublicationIncompleteSequenceCardDetails()
              ..title = localSequence.name
              ..localDetails = localSequence
              ..numberOfLocalPictures = localSequence.pictures.length
              ..numberOfPublishedPictures = 0
              ..creationDate = localSequence.creationDate
        );
      }
    });

    listOfSequenceCardDetails.sort(
      (a, b) => b.creationDate.compareTo(a.creationDate)
    );

    return listOfSequenceCardDetails;
  }

  static Future<void> openSequenceInBrowser(String sequenceId) async {
    final instance = await StorageService.getSelectedInstanceUrl();
    final Uri url = Uri.https(instance!, '/sequence/${sequenceId}');
    if (!await launchUrl(url)) {
      throw Exception("Could not launch $url");
    }
  }

  static Future<void> shareSequence(BuildContext context, String sequenceId) async {
    final instance = await StorageService.getSelectedInstanceUrl();
    final url = "$instance/sequence/${sequenceId}";
    await Share.share(url,
        subject: AppLocalizations.of(context)!.titleShareUrl);
  }

  static Future<void> goToCapture({int? sequenceToEditId}) async {
    if (!await PermissionHelper.isPermissionGranted()) {
      await PermissionHelper.askMissingPermission();
    }
    if (await PermissionHelper.isPermissionGranted()) {
      await availableCameras().then((availableCameras) =>
        GetIt.instance<NavigationService>().pushTo(
          Routes.newSequenceCapture,
          arguments: {
            'sequenceToEditId': sequenceToEditId,
            'cameras': availableCameras
          }
        )
      );
    }
  }
}
