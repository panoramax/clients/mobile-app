part of panoramax;

class ImportPictureService {
  static Future<void> importPictures(BuildContext context) async {
    FilePickerResult? pickerResult = await FilePicker.platform.pickFiles(
        type: FileType.image,
        allowCompression: false,
        allowMultiple: true
    );
    if(pickerResult != null) {
      SequenceDto sequenceToCreate = await createSequenceDtoWithPictures(pickerResult.xFiles, context);
      goToSequenceUploadPage(sequenceToCreate);
    }
  }

  static Future<SequenceDto> createSequenceDtoWithPictures(List<XFile> selectedFiles, BuildContext context) async {
    SequenceDto sequenceToCreate = await CaptureService.createSequenceDto(context);
    for (var pic in selectedFiles) {
      sequenceToCreate = await CaptureService.addImageSequence(
          sequenceToCreate,
          pic,
          doesImageHaveCorrectExifTags: await doesImageHaveCorrectExifTags(pic)
      );
    }
    return sequenceToCreate;
  }

  static void goToSequenceUploadPage(SequenceDto sequenceToCreate) {
    GetIt.instance<NavigationService>().pushTo(
        Routes.newSequenceUpload,
        arguments: {
          'sequenceToCreateId': sequenceToCreate.id,
          'sendOnStart': false
        }
    );
  }

  static Future<bool> doesImageHaveCorrectExifTags (XFile picture) async {
    final exif = await Exif.fromPath(picture.path);
    final gpsLatLong = await exif.getLatLong();
    final date = await exif.getOriginalDate();
    if (gpsLatLong == null || date == null) {
      return false;
    }
    return true;
  }
}
