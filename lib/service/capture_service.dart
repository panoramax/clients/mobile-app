part of panoramax;

class CaptureService {

  static Future<SequenceDto> createSequenceDto(BuildContext context) {
    var creationDate = DateTime.now();
    final collectionName = getSequenceName(context, creationDate);
    return SequenceRepository.createSequence(
        collectionName,
        creationDate
    );
  }

  static String getSequenceName(BuildContext context, DateTime creationDate) =>
      "${AppLocalizations.of(context)!.newSequenceNamePreffix} ${DateFormat(AppLocalizations.of(context)!.newSequenceNameDateFormat).format(creationDate)}";

  static Future<SequenceDto> addImageSequence(SequenceDto sequenceToEnriched, XFile rawImage,
      {doesImageHaveCorrectExifTags = true}) async {
    sequenceToEnriched.pictures.add(
        SequencePictureDto()
          ..localFilePath = rawImage.path
          ..doesImageHaveCorrectExifTags = doesImageHaveCorrectExifTags
    );
    return SequenceRepository.updateSequence(sequenceToEnriched);
  }

  static CaptureButtonState getCaptureButtonState(Position? currentPosition, double? accuracy, bool isBurstMode, bool isBurstPlay, bool isProcessing) {
    if (accuracy == null) {
      return CaptureButtonState.GPS_NOT_GRANTED;
    }
    if (currentPosition == null) {
      return CaptureButtonState.GPS_IN_ACQUISITION;
    }
    if (!isGpsAccuracyGood(accuracy)) {
      return CaptureButtonState.GPS_NOT_PRECISE;
    }
    if (isBurstMode) {
      return (isBurstPlay) ? CaptureButtonState.SEQUENCE_PHOTO_IN_PROGRESS : CaptureButtonState.SEQUENCE_PHOTO_AVAILABLE;
    } else {
      return (isProcessing) ? CaptureButtonState.SINGLE_PHOTO_IN_PROGRESS : CaptureButtonState.SINGLE_PHOTO_AVAILABLE;
    }
  }

  static bool isGpsAccuracyGood(double? gpsAccuracy) {
    return gpsAccuracy != null && gpsAccuracy < GPS_ACCURACY_THRESHOLD;
  }
}
