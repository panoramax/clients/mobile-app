part of panoramax;

class SequenceRepository {
   static late Box<SequenceDto> instance;

   static Future<void> initializeDatabase() async {
     objectbox = await ObjectBox.create();
     instance = objectbox.store.box<SequenceDto>();
   }

   static Future<SequenceDto> createSequence(String name, DateTime creationDate) {
     return instance.putAndGetAsync(
         SequenceDto()
           ..name = name
           ..creationDate = creationDate
     );
   }

   static SequenceDto? getSequence(int idSequence) {
     return instance.get(idSequence);
   }

   static List<SequenceDto> getAllSequences() {
     return instance.getAll();
   }

   static Future<SequenceDto> updateSequence(SequenceDto sequenceToUpdate) {
     return instance.putAndGetAsync(sequenceToUpdate);
   }
}