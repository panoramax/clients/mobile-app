part of panoramax;

@Entity()
class SequenceDto {
  @Id()
  int id = 0;
  String? panoramaxSequenceId;
  late String name;
  late DateTime creationDate;

  @Backlink('sequence')
  final pictures = ToMany<SequencePictureDto>();

  @override
  String toString() {
    return 'SequenceDto{id: $id, panoramaxSequenceId: $panoramaxSequenceId, name: $name, creationDate: $creationDate, pictures: $pictures}';
  }
}

@Entity()
class SequencePictureDto {
  @Id()
  int id = 0;
  late String localFilePath;
  bool isToSend = true;
  bool hasBeenSent = false;
  bool doesImageHaveCorrectExifTags = true;
  final sequence = ToOne<SequenceDto>();

  @override
  String toString() {
    return 'SequencePictureDto{id: $id, localFilePath: $localFilePath, isToSend: $isToSend, hasBeenSent: $hasBeenSent, doesImageHaveCorrectExifTags: $doesImageHaveCorrectExifTags}';
  }
}