part of panoramax;

class Logger {
  static final Talker _instance = initTalker();

  static Talker initTalker() {
    var talkerSettings = TalkerSettings(
      useHistory: true,
      enabled: true,
      timeFormat: TimeFormat.yearMonthDayAndTime
    );
    final talker = Talker(
      settings: talkerSettings,
      logger: TalkerLogger(
        settings: TalkerLoggerSettings(),
      ),
      history: CustomHistory(talkerSettings)
    );
    return talker;
  }

  static Talker getInstance() => _instance;
}

extension TalkerDataExtension on TalkerData {
  /// Convert `TalkerData` to JSON
  Map<String, dynamic> toJson() {
    return {
      'message': message,
      'logLevel': logLevel?.name,
      'error': error?.toString(),
      'exception': exception?.toString(),
      'stackTrace': stackTrace?.toString(),
      'time': time.toIso8601String(),
    };
  }

  /// Convert JSON to `TalkerData`
  static TalkerData fromJson(Map<String, dynamic> json) {
    return TalkerData(
      json['message'] ?? 'No message',
      logLevel: LogLevel.values.firstWhere(
            (e) => e.name == json['logLevel'],
        orElse: () => LogLevel.info,
      ),
      error: Error(),
      exception: json['exception'] != null ? Exception(json['exception']) : null,
      stackTrace: json['stackTrace'] != null ? StackTrace.fromString(json['stackTrace']) : null,
      time: DateTime.parse(json['time']),
    );
  }
}

class CustomHistory implements TalkerHistory {
  final TalkerSettings settings;
  final List<TalkerData> _history = [];

  @override
  List<TalkerData> get history => _history;

  CustomHistory(this.settings, {List<TalkerData>? history}) {
    loadHistory(history: history);
  }

  loadHistory({List<TalkerData>? history}) async {
    final File logFile = await getLogFile();
    if (!logFile.existsSync()) {
      logFile.createSync();
    }
    _history.addAll(await readTalkerDataFromFile());
    if (history != null) {
      /// when initialize talker i set List which i write in file
      _history.addAll(history);
    }
  }

  Future<File> getLogFile() async {
    final Directory docDir = await getApplicationDocumentsDirectory();
    return File(path.join(docDir.path, 'history.log'));
  }

  @override
  void clean() async {
    if (settings.useHistory) {
      _history.clear();
      final File logFile = await getLogFile();
      if (logFile.existsSync()) {
        logFile.deleteSync();
      }
    }
  }

  @override
  void write(TalkerData data) {
    if (settings.useHistory && settings.enabled) {
      if (settings.maxHistoryItems <= _history.length) {
        _history.removeAt(0);
      }
      _history.add(data);
      _writeToFile(data);
    }
  }

  void _writeToFile(TalkerData newLog) async {
    final File file = await getLogFile();

    final String jsonEntry = jsonEncode(newLog.toJson());

    final currentContent = await readTalkerDataFromFile();

    // Append the new log entry with a newline
    await file.writeAsString('$jsonEntry\n', mode: FileMode.append, flush: true);
  }

  Future<List<TalkerData>> readTalkerDataFromFile() async  {
    final File file = await getLogFile();

    if (!file.existsSync()) {
      return [];
    }

    final List<String> lines = await file.readAsLines();

    return lines
        .where((line) => line.trim().isNotEmpty) // Ignore empty lines
        .map((line) => TalkerDataExtension.fromJson(jsonDecode(line))) // Parse each line separately
        .toList();
  }
}