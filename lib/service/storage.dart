part of panoramax;

const selectedInstanceKey = "selectedInstance";
const accessTokenKey = "accessToken";
const isOnboardingDoneKey = "isOnboardingDone";
const currentLangKey = "lang";
const currentThemeKey = "theme";
const currentIsFlashTurnOnKey = "isFlashTurnOn";
const currentReduceBrightnessOnCaptureKey = "reduceBrightnessOnCapture";
const currentSentViaWifiOnlyKey = "sentViaWifiOnly";

class StorageService {
  static Future<String?> getSelectedInstanceUrl() async {
    return _getSharedPreference(selectedInstanceKey);
  }

  static void setSelectedInstance(String instance) async {
    _setSharedPreference(selectedInstanceKey, instance);
  }

  static Future<String?> getToken() async {
    return await _getSharedPreference(accessTokenKey);
  }

  static void setToken(String token) async {
    _setSharedPreference(accessTokenKey, token);
  }

  static Future<bool> isOnboardingDone() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(isOnboardingDoneKey) == true;
  }

  static void setOnboardingDone() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(isOnboardingDoneKey, true);
  }

  static void setLang(String lang) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(currentLangKey, lang);
  }

  static Future<String> getLang() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(currentLangKey) ?? 'fr';
  }

  static void setTheme(String theme) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(currentThemeKey, theme);
  }

  static Future<ThemeMode> getTheme() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return Future.value(ThemeMode.values.firstWhereOrNull((element) => element.name == prefs.getString(currentThemeKey)) ?? ThemeMode.light);
  }

  static void setIsFlashTurnOn(bool isFlashTurnOn) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(currentIsFlashTurnOnKey, isFlashTurnOn);
  }

  static Future<bool?> getIsFlashTurnOn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(currentIsFlashTurnOnKey);
  }

  static void setReduceBrightnessOnCapture(bool reduceBrightnessOnCapture) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(currentReduceBrightnessOnCaptureKey, reduceBrightnessOnCapture);
  }

  static Future<bool?> getReduceBrightnessOnCapture() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(currentReduceBrightnessOnCaptureKey);
  }

  static void setSentViaWifiOnly(bool sentViaWifiOnly) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(currentSentViaWifiOnlyKey, sentViaWifiOnly);
  }

  static Future<bool?> getSentViaWifiOnly() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(currentSentViaWifiOnlyKey);
  }

  static Future _setSharedPreference(String key, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String?> _getSharedPreference(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<Directory> getImageFolder() async {
    final rootFolder = Directory(
      path.join(
        (await ExternalPath.getExternalStorageDirectories())[0],
        ExternalPath.DIRECTORY_DCIM,
        "Panoramax"
      )
    );
    if(!rootFolder.existsSync()) {
      rootFolder.createSync(recursive: true);
    }
    return rootFolder;
  }
}

