import 'dart:io';
import '../main.dart';
import 'api/api.dart';
import 'api/model/geo_visio.dart';

class CollectionService {
  static Future<GeoVisioCollection?> getUserCollections() async {
    return CollectionsApi.INSTANCE.getMeCollection();
  }

  static Stream<int> uploadSequencesToServer(SequenceDto sequence) async* {
    Logger.getInstance().info("Upload sequences to server requested");
    var panoramaxSequenceId = sequence.panoramaxSequenceId ?? await _createRemoteCollection(sequence.name);
    sequence.panoramaxSequenceId = panoramaxSequenceId;

    final picturesToSend = sequence.pictures
        .where((p) => !p.hasBeenSent && p.isToSend)
        .toList();
    for (var i = 0; i < picturesToSend.length; i++) {
      Logger.getInstance().debug("Uploading image ${picturesToSend[i].localFilePath}");
      await CollectionsApi.INSTANCE.apiCollectionsUploadPicture(
          collectionId: sequence.panoramaxSequenceId!,
          position: i + 1,
          pictureToUpload: getImageFileFromSequencePicture(picturesToSend[i])
      );
      Logger.getInstance().debug("Image ${picturesToSend[i].localFilePath} uploaded");
      yield i+1;
    }

    await SequenceRepository.updateSequence(sequence);
    Logger.getInstance().info("Upload sequences to server done");
  }

  static Future<String> _createRemoteCollection(String collectionName) async {
    try {
      Logger.getInstance().debug("Creating remote sequence");
      final collection = await CollectionsApi.INSTANCE
          .apiCollectionsCreate(newCollectionName: collectionName);
      Logger.getInstance().debug("Sequence ${collection.id} ${collectionName} created");
      return collection.id;
    } catch (e) {
      rethrow;
    }
  }

  static File getImageFileFromSequencePicture(SequencePictureDto sequencePicture) {
    return File(sequencePicture.localFilePath);
  }

  static void deleteUnselectedPictures(SequenceDto sequenceToCreate) {
    sequenceToCreate.pictures
        .removeWhere((picture) => !picture.isToSend || !picture.doesImageHaveCorrectExifTags);
  }
}