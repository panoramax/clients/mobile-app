part of panoramax;

class Routes extends Equatable {
  static const String homepage = "home";
  static const String onboarding = "onboarding";
  static const String newSequenceCapture = "new-sequence/capture";
  static const String newSequenceUpload = "new-sequence/upload";
  static const String instance = "instance";
  static const String settings = "settings";
  static const String logs = "logs";

  @override
  List<Object?> get props => [
    homepage,
    onboarding,
    newSequenceCapture,
    newSequenceUpload,
    instance,
    settings,
    logs
  ];
}

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  dynamic pushTo(String route, {dynamic arguments}) {
    return navigatorKey.currentState?.pushNamed(route, arguments: arguments);
  }

  dynamic popAndPush(String route, {dynamic arguments}) {
    return navigatorKey.currentState?.popAndPushNamed(route, arguments: arguments);
  }

  dynamic pushReplacementTo(String route, {dynamic arguments}) {
    return navigatorKey.currentState
        ?.pushReplacementNamed(route, arguments: arguments);
  }

  dynamic goHome() {
    return navigatorKey.currentState?.pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => HomePage()),
        ModalRoute.withName(Routes.homepage)
    );
  }
}

void setSystemChromeUISettings(RouteSettings settings) {
  if (settings.name == Routes.newSequenceCapture) {
    OrientationService.allowPortraitAndLandscape();
  } else {
    OrientationService.allowOnlyPortrait();
  }
}

Route<dynamic> generateRoutes(RouteSettings settings) {
  setSystemChromeUISettings(settings);
  switch (settings.name) {
    case "home":
      return MaterialPageRoute(
        builder: (_) => HomePage(),
        settings: settings
      );
    case "onboarding":
      return MaterialPageRoute(
        builder: (_) => OnboardingPage(),
        settings: settings
      );
    case "new-sequence/capture":
      return MaterialPageRoute(
        builder: (_) => CapturePage(
          cameras: (settings.arguments as Map<String, Object?>)['cameras'] as List<CameraDescription>,
          sequenceToEditId: (settings.arguments as Map<String, Object?>)['sequenceToEditId'] as int?
        ),
        settings: settings
      );
    case "new-sequence/upload":
      return MaterialPageRoute(
        builder: (_) => CollectionUploadPage(
          sequenceToCreateId: (settings.arguments as Map<String, Object>)['sequenceToCreateId'] as int,
          sendOnStart: (settings.arguments as Map<String, Object>)['sendOnStart'] as bool
        ),
        settings: settings
      );
    case "instance":
      return MaterialPageRoute(
        builder: (_) => InstancePage(
          sequenceToCreateId: settings.arguments as int
        ),
        settings: settings
      );
    case "settings":
      return MaterialPageRoute(
        builder: (_) => SettingPage(),
        settings: settings
      );
    case "logs":
      return MaterialPageRoute(
        builder: (context) => TalkerScreen(
          talker: Logger.getInstance(),
          theme: TalkerScreenTheme(
            backgroundColor: Theme.of(context).colorScheme.primaryContainer
          ),
        ),
        settings: settings
      );
    default:
      return MaterialPageRoute(builder: (_) => HomePage());
  }
}
