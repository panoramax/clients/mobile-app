import 'package:get_it/get_it.dart';

import '../main.dart';

final class OnboardingService {
  static Future<bool> isOnboardingAlreadyDone() async {
    return StorageService.isOnboardingDone();
  }

  static Future<void> terminateOnboarding() async {
    Logger.getInstance().info("Onboarding done");
    StorageService.setOnboardingDone();
    _goToHomepage();
  }

  static Future<void> _goToHomepage() async {
    GetIt.instance<NavigationService>().pushReplacementTo(Routes.homepage);
  }
}