part of panoramax;

abstract class ThemeButton {
  static OutlinedButtonThemeData get() => OutlinedButtonThemeData(
      style: ButtonStyle(
          iconSize: WidgetStateProperty.all(25),
          minimumSize: WidgetStateProperty.all(Size(150, 50)),
          backgroundColor: WidgetStateProperty.all(PrimaryColor),
          textStyle: WidgetStateProperty.all(ThemeText.get().titleMedium)));
}
