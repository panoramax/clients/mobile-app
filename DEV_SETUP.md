# Development environment setup
This project is developed in Flutter.

## Prerequisites
- One of the following Development IDE: Android Studio, IntelliJ or VS Code
- **Flutter** version 3.24.5 or later :
    - [Install Flutter Android app on windows](https://docs.flutter.dev/get-started/install/windows/mobile)
    - [Install Flutter Android app on Linux](https://docs.flutter.dev/get-started/install/linux/android)
    - [Install Flutter IOS app on MacOS](https://docs.flutter.dev/get-started/install/macos/mobile-ios)

## Create your account on development server
Create an account if you don't already have one on the development server by following this url : [Panoramax](https://panoramax.codeureusesenliberte.fr) > `Connexion | Inscription` > `Register`

## Setup workspace
1. Retrieve sources
     ```shell
     git clone https://gitlab.com/panoramax/clients/mobile-app.git
     ```

2. Install dependencies
    ```shell
    flutter pub get
    ```

## Execute application
### With Jetbrain IDEs(Android Studio, IntelliJ, etc)
First install [Flutter Plugin](https://plugins.jetbrains.com/plugin/9212-flutter)<br>
Here are the declared runConfigurations:

| runConfiguration | Description                                                                                                                                       |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| runDev           | Launch the application in development environment: the panoramax instance available is [Panoramax Dev](https://panoramax.codeureusesenliberte.fr) |
| runProd          | Launch the application in production environment : the same instances as for production are available                                             |


#### On VsCode
First install [Flutter Extension](https://code.visualstudio.com/docs/editor/extension-marketplace#_search-for-an-extension)<br>
Here are the declared Launch tasks:

| Launch task | Description                                                                                                                                       |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| runDev           | Launch the application in development environment: the panoramax instance available is [Panoramax Dev](https://panoramax.codeureusesenliberte.fr) |
| runProd          | Launch the application in production environment : the same instances as for production are available                                             |       

## Package app
### Build apk for Android
```shell  
flutter build apk --release
```
### Build apk for IOS
```shell  
flutter build ipa --release
```

## Generate translations
```shell
flutter clean
flutter pub get
```

## Git Commit Conventions
Git commits in this repository shall be:
1. **atomic** (1 commit `=` 1 and only 1 _thing_),
2. **semantic** (using [semantic-release commit message syntax](https://semantic-release.gitbook.io/semantic-release/#commit-message-format)).
3. **pattern**
    - **SCOPE**: one of (build, chore, ci, docs, feat, fix, perf, refactor, revert, style, test, release)
    - **PACKAGE** _(optional)_: one of (a11y, assistant, assistant-electron, docs, intellij-plugin, runner-commons, runner-cypress, runner-playwright)
    - **Pattern**: SCOPE(PACKAGE): commit message, #issue_identifier
   ```text
   feat(runner-cypress): optimize bdd request, #51

## Automated Tests
### Generate integration tests
```shell  
dart run build_runner build --delete-conflicting-outputs
```

### Watch integration tests
```shell  
dart run build_runner watch --delete-conflicting-outputs
```

### Run integration tests
```shell  
flutter test integration_test
```
