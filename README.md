<p align="center">
<img src="/assets/logo-mobile-app-beta.png" height="100"/>
</p>
<h1 align="center">Panoramax Mobile App</h1>

Panoramax mobile is an application to capture photo sequences with your mobile and send these sequences to a Panoramax instance.


> The app is under development, **[you can download the last test version for Android](https://gitlab.com/api/v4/projects/57815321/jobs/artifacts/main/raw/build/app/outputs/flutter-apk/app-debug.apk?job=android)**.
>
> You can follow progression in this [project board](https://gitlab.com/panoramax/clients/mobile-app/-/boards).

<div align="center">
  <!--- <a href="https://f-droid.org/packages/app_id"> 
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80" align="center"/>
  </a>
  <!--- <a href="https://play.google.com/store/apps/app_id">
    <img src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' alt='Get it on Google Play' height="80" align="center"/>
  </a>
  <a href="https://apps.apple.com/us/app/app_id">
    <img src="assets/apple-appstore.svg" alt="Get it on App Store" height="60" align="center"/>
  </a>--->

</div>

## Documentation
- [Panoramax website](https://panoramax.fr/)
- [Meta catalog](https://api.panoramax.xyz/)
- [Panoramax repository](https://gitlab.com/panoramax/)
- [Panoramax Discourse forum](https://forum.geocommuns.fr/c/panoramax/6)

## Contribute to the code
Please refer to [Contributing documentation](./CONTRIBUTING.md) for information on how to contribute.<br>
In addition, [Development environment setup](./DEV_SETUP.md) describes how to set up the local development environment

## License
Licensed under the AGPL-3.0 license. See [LICENSE](/LICENSE)
